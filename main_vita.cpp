#include <platform/vita/system/platform_vita.h>

#include "GameApp.h"

unsigned int sceLibcHeapSize = 128*1024*1024;	// Sets up the heap area size as 128MiB.

#pragma comment(lib, "SceCamera_stub")
#pragma comment(lib, "SceMotion_stub")

int main(void)
{
	// Initialisation
	gef::PlatformVita platform;

	GameApp myApp(platform);
	myApp.Run();

	return 0;
}