#include "SplashState.h"

// --CONSTRUCTORS--

ds::state::SplashState::SplashState() {
}

ds::state::SplashState::~SplashState() {
}

// -----PUBLIC-----

// Initialises the splash state.
void ds::state::SplashState::Initialise() {
	// Set up sprites.
	textureHandler->SetUpSprite(&background,
		"Background",
		gef::Vector4(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2, 0),
		gef::Vector2(DISPLAY_WIDTH, DISPLAY_HEIGHT));

	textureHandler->SetUpSprite(&devLogo,
		"DeveloperLogo",
		gef::Vector4(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2, 0),
		gef::Vector2(1024, 1024));


	yearText = "2016";
	timePassed = 0;
}

// Updates the splash state.
void ds::state::SplashState::Update(float deltaTime, AppData* dat) {
	// Update the amount of time passed.
	timePassed += deltaTime;

	// Change the state if the controller receives input, or a fixed amount of time has passed.
	if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_START ||
		inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_CROSS ||
		timePassed >= 3.0f) {
		changeState = true;
	}
}

// Renders the splash state.
void ds::state::SplashState::Render(AppData* dat) {
	// Start rendering 2D.
	spriteRenderer->Begin(true);

	// Render the background.
	spriteRenderer->DrawSprite(background);

	// Render the developer logo.
	spriteRenderer->DrawSprite(devLogo);

	// Render the development year.
	font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH, DISPLAY_HEIGHT - 30, 0), 0.75f, 0xffffffff, gef::TextJustification::TJ_RIGHT, yearText.c_str());

	// Stop rendering 2D.
	spriteRenderer->End();
}

// Releases any used resources.
void ds::state::SplashState::CleanUp() {
}

// Returns the state to change to.
ds::state::GameState ds::state::SplashState::GetNextState() {
	return GameState::GS_MenuState;
}
