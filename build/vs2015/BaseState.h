#ifndef BASESTATE_H
#define BASESTATE_H

#include <string>

#include <graphics/font.h>
#include <graphics/renderer_3d.h>
#include <graphics/sprite_renderer.h>
#include <input/input_manager.h>
#include <input/keyboard.h>
#include <input/sony_controller_input_manager.h>
#include <sony_sample_framework.h>
#include <system/platform.h>

#include "ARCamera.h"
#include "AudioHandler.h"
#include "Debug.h"
#include "ModelHandler.h"
#include "TextureHandler.h"

namespace ds {
namespace state {

// Struct used to pass handlers to the states.
// ================
struct StatePointers {
	gef::Platform* platform;
	gef::SpriteRenderer* spriteRenderer;
	gef::Renderer3D* renderer3D;
	gef::InputManager* inputManager;
	ds::AudioHandler* audioHandler;
	ds::ModelHandler* modelHandler;
	ds::TextureHandler* textureHandler;
	ds::utils::Debug* debugOverlay;
	ds::ARCamera* arCamera;

	gef::Font* font;
};

// Game state enum.
// ================
enum GameState {
	GS_SplashState,
	GS_MenuState,
	GS_InGameState,
	GS_GameOverState,
	GS_CreditsState
};

// Base game state machine state.
// ================
class BaseState {
public:
	BaseState();
	virtual ~BaseState() = 0;

	virtual void Initialise() = 0;
	virtual void Update(float deltaTime, AppData* dat) = 0;
	virtual void Render(AppData* dat) = 0;
	virtual void CleanUp() = 0;
	virtual GameState GetNextState() = 0;

	void LinkStatePointers(StatePointers statePointers);

	bool changeState;
	bool initialised;
	bool usesMarkers;

protected:
	void CleanUpStatePointers();

	// Handlers
	gef::Platform* platform;
	gef::SpriteRenderer* spriteRenderer;
	gef::Renderer3D* renderer3D;
	gef::InputManager* inputManager;
	ds::AudioHandler* audioHandler;
	ds::ModelHandler* modelHandler;
	ds::TextureHandler* textureHandler;
	ds::utils::Debug* debugOverlay;
	ds::ARCamera* arCamera;

	gef::Font* font;
};

}
}
#endif