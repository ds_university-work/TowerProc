#include "Level.h"

// --CONSTRUCTORS--

ds::Level::Level() {
}

ds::Level::~Level() {
}

// -----PUBLIC-----

// Initialises the level.
void ds::Level::Initialise(ModelHandler* modelHandler_, ARCamera* arCamera_, gef::Vector4* worldScale_) {
	// Link handlers.
	modelHandler = modelHandler_;
	arCamera = arCamera_;
	worldScale = worldScale_;

	// Set up home object.
	homeObject = GameObject3D(modelHandler->GetMesh("HomeObject"), &arCamera->GetMarker(AR_MARKER_1)->transform, gef::Vector4(0, 0, 0.01f), worldScale, false);
	homeObject.SetScale(gef::Vector4(0.5f, 0.5f, 0.5f));

	// Initialise variables.
	allEnemiesSpawned = false;
	waveEnemiesKilled = 0;
}

// Updates game objects.
void ds::Level::UpdateObjects(float deltaTime) {
	// Update grid tiles.
	for (int y = 0; y < grid.size(); y++) {
		for (int x = 0; x < grid[y].size(); x++) {
			grid[y][x].Update(deltaTime);
		}
	}

	// Update home object.
	homeObject.Update(deltaTime);

	// Update enemy spawn objects.
	for (int i = 0; i < enemySpawnerObjects.size(); i++) {
		enemySpawnerObjects[i].Update(deltaTime);
	}

	// Update towers.
	for (int i = 0; i < towers.size(); i++) {
		towers[i].Update(deltaTime);
		towers[i].top.Update(deltaTime);
	}

	// Update projectiles.
	for (int i = 0; i < towerProjectiles.size(); i++) {
		if (towerProjectiles[i].destroy) {
			towerProjectiles.erase(towerProjectiles.begin() + i);
			i--;
		} else {
			towerProjectiles[i].Update(deltaTime);
		}
	}

	// Update enemies.
	for (int i = 0; i < enemies.size(); i++) {
		if (enemies[i].destroy) {
			enemies.erase(enemies.begin() + i);
			i--;
		} else {
			enemies[i].Update(deltaTime);
		}
	}
}

// Renders game objects.
void ds::Level::Render(gef::Renderer3D* renderer3D) {
	// Render grid tiles.
	for (int y = 0; y < grid.size(); y++) {
		for (int x = 0; x < grid[y].size(); x++) {
			if (grid[y][x].GetTileType() == TileType::Ground) {
				renderer3D->DrawMesh(grid[y][x]);
			}
		}
	}

	// Render home object.
	renderer3D->DrawMesh(homeObject);

	// Render enemy spawn objects.
	for (int i = 0; i < enemySpawnerObjects.size(); i++) {
		renderer3D->DrawMesh(enemySpawnerObjects[i]);
	}

	// Render towers.
	for (int i = 0; i < towers.size(); i++) {
		renderer3D->DrawMesh(towers[i]);
		renderer3D->DrawMesh(towers[i].top);
	}

	// Render projectiles.
	for (int i = 0; i < towerProjectiles.size(); i++) {
		renderer3D->DrawMesh(towerProjectiles[i]);
	}

	// Render enemies.
	for (int i = 0; i < enemies.size(); i++) {
		renderer3D->DrawMesh(enemies[i]);
	}
}

// Sets the level grid.
void ds::Level::SetGrid(std::vector<std::vector<ds::TileGameObject>> levelGrid) {
	grid = levelGrid;
}

// Sets the home tile.
void ds::Level::SetHome(gef::Vector2 home) {
	homeCoordinate = home;

	homeObject.SetWorldAnchor(grid[home.y][home.x].GetWorldTransform(), false);
}

// Adds or removes an enemy spawner.
bool ds::Level::AddRemoveEnemySpawn(gef::Vector2 spawn) {
	bool removedSpawner = false;

	// Check the placed spawns and remove any matching.
	for (int i = 0; i < enemySpawnerCoordinates.size(); i++) {
		if (enemySpawnerCoordinates[i].x == spawn.x && enemySpawnerCoordinates[i].y == spawn.y) {
			enemySpawnerCoordinates.erase(enemySpawnerCoordinates.begin() + i);
			enemySpawnerObjects.erase(enemySpawnerObjects.begin() + i);
			removedSpawner = true;
			break;
		}
	}

	// If no spawns have been removed, then add one.
	if (!removedSpawner) {
		enemySpawnerCoordinates.push_back(spawn);

		// Create matching spawner object
		enemySpawnerObjects.push_back(ds::GameObject3D(modelHandler->GetMesh("EnemyObject"), grid[spawn.y][spawn.x].GetWorldTransform(), gef::Vector4(0, 0, 0.005f), worldScale, false));
		enemySpawnerObjects.back().SetScale(gef::Vector4(1, 1, 1));
	}

	// Return true if a spawner has been added.
	return !removedSpawner;
}

// Updates the anchoring of the grid and game objects.
void ds::Level::UpdateGridAnchoring(bool manual) {
	if (arCamera->gridMarkerVisibilityChange || manual) {
		// Marker distances
		gef::Vector2 distance = arCamera->GetGridMarkerDistance();
		
		// Get marker visability.
		bool m1 = arCamera->GetMarker(AR_MARKER_1)->visible;
		bool m2 = arCamera->GetMarker(AR_MARKER_2)->visible;

		// Get distance between tiles.
		gef::Vector4 currentTileDistance = grid[gridSize.y - 1][gridSize.x - 1].GetPosition() - grid[0][0].GetPosition();
		gef::Vector2 newTileDistance = gef::Vector2(distance.x / (gridSize.x * 2 - 2), distance.y / (gridSize.y * 2 - 2));

		if (m1 && m2) {
			// If both markers are visible, anchor grid to the first marker.
			// NOTE: Ideally would anchor grid to both markers, finding the average transform between anchor points.
			for (int y = 0; y < gridSize.y * 2 - 1; y++) {
				for (int x = 0; x < gridSize.x * 2 - 1; x++) {
					grid[y][x].SetWorldAnchor(&arCamera->GetMarker(AR_MARKER_1)->transform);
					grid[y][x].SetPosition(gef::Vector4(x * newTileDistance.x,
														y * newTileDistance.y,
														grid[y][x].GetPosition().z()));
				}
			}

			// Anchor enemies to first marker.
			for (int i = 0; i < enemies.size(); i++) {
				enemies[i].SetWorldAnchor(&arCamera->GetMarker(AR_MARKER_1)->transform);
				enemies[i].UpdateStartPosition();
			}
		} else if (m1 && !m2) {
			// If only the first marker is visible, anchor grid to that marker.
			for (int y = 0; y < gridSize.y * 2 - 1; y++) {
				for (int x = 0; x < gridSize.x * 2 - 1; x++) {
					grid[y][x].SetWorldAnchor(&arCamera->GetMarker(AR_MARKER_1)->transform);
					grid[y][x].SetPosition(gef::Vector4(x * newTileDistance.x,
														y * newTileDistance.y,
														grid[y][x].GetPosition().z()));
				}
			}

			// Anchor enemies to first marker.
			for (int i = 0; i < enemies.size(); i++) {
				enemies[i].SetWorldAnchor(&arCamera->GetMarker(AR_MARKER_1)->transform);
				enemies[i].UpdateStartPosition();
			}
		} else if (!m1 && m2) {
			// If only the second marker is visible, anchor grid to that marker.
			for (int y = 0; y < gridSize.y * 2 - 1; y++) {
				for (int x = 0; x < gridSize.x * 2 - 1; x++) {
					grid[y][x].SetWorldAnchor(&arCamera->GetMarker(AR_MARKER_2)->transform);
					grid[y][x].SetPosition(gef::Vector4(x * newTileDistance.x - arCamera->GetGridMarkerDistance().x,
														y * newTileDistance.y - arCamera->GetGridMarkerDistance().y,
														grid[y][x].GetPosition().z()));
				}
			}

			// Anchor enemies to second marker.
			for (int i = 0; i < enemies.size(); i++) {
				enemies[i].SetWorldAnchor(&arCamera->GetMarker(AR_MARKER_2)->transform);
				enemies[i].UpdateStartPosition();
			}
		}

		// Update tile scaling.
		gef::Vector4 tileScale;
		tileScale = gef::Vector4((distance.x / (gridSize.x)), (distance.y / (gridSize.y )), 1);

		// Set the world scale.
		worldScale->set_value(0.28f * tileScale.x(), 0.3f * tileScale.y(), 0.01f * tileScale.z());
	}
}

// Sets up the enemies to be spawned during the next wave.
int ds::Level::SetEnemiesToSpawn(int wave) {
	int count = GetNumberOfWaveEnemies(wave);

	for (int i = 0; i < count; i++) {
		// Create new enemy to spawn.
		enemiesToSpawn.push_back(EnemyStruct());

		// Set enemy attributes.
		enemiesToSpawn.back().speed = 1.0f + ((float)wave * 0.4f);
		enemiesToSpawn.back().speed += (rand() % 11 * 0.1f * enemiesToSpawn.back().speed);
		enemiesToSpawn.back().health = 2.0f + ((float)wave * 0.4f);

		// Set enemy spawn origin.
		int spawnIndex = i % enemySpawnerCoordinates.size();
		enemiesToSpawn.back().spawnTile = &grid[enemySpawnerCoordinates[spawnIndex].y][enemySpawnerCoordinates[spawnIndex].x];

		// Set enemy spawn path.
		std::vector<ds::TileGameObject*> path;
		for (int i = 0; i < enemyPath.size(); i++) {
			path.push_back(&grid[enemyPath[i].y][enemyPath[i].x]);
		}
		enemiesToSpawn.back().path = path;
	}

	allEnemiesSpawned = false;

	return count;
}

// Spawns an enemy.
void ds::Level::SpawnEnemy() {
	// Check if there are any enemies to spawn.
	if (enemiesToSpawn.size() == 0) {
		allEnemiesSpawned = true;
		return;
	}

	// Spawn enemy.
	enemies.push_back(Enemy(modelHandler->GetMesh("Enemy"), &arCamera->GetMarker(AR_MARKER_1)->transform, gef::Vector4(0, 0, 0.01f), worldScale));
	enemies.back().Initialise(enemiesToSpawn.back().path, enemiesToSpawn.back().speed, enemiesToSpawn.back().health);
	enemies.back().SetScale(gef::Vector4(0.1f, 0.1f, 0.1f));
	
	// Remove enemy from list to spawn.
	enemiesToSpawn.pop_back();
}

// Updates the combat.
void ds::Level::CombatUpdate(int* health) {
	// Check if projectiles have collided with enemies.
	for (int p = 0; p < towerProjectiles.size(); p++) {
		if (!towerProjectiles[p].destroy) {
			for (int e = 0; e < enemies.size(); e++) {
				// If the projectile has collided with the enemy.
				if (ds::utils::CheckCollisionAABB(&towerProjectiles[p], &enemies[e])) {
					// Damage the enemy.
					towerProjectiles[p].DamageEnemy();
					break;
				}
			}
		}
	}

	// Update tower targets.
	for (int t = 0; t < towers.size(); t++) {
		towers[t].UpdateTarget(&enemies);
	}

	// Check if enemies have reached the home.
	for (int i = 0; i < enemies.size(); i++) {
		float distance = ds::utils::GetDistanceBetweenPoints(enemies[i].GetPosition(false), homeObject.GetPosition(false));
		if (distance < 0.015f) {
			// Damage the player and destroy the enemy.
			*health -= 1;
			enemies[i].destroy = true;
		}
	}

	// Remove killed enemies.
	for (int i = 0; i < enemies.size(); i++) {
		if (enemies[i].health <= 0) {
			waveEnemiesKilled++;
			enemies.erase(enemies.begin() + i);
			i--;
		}
	}

}

// Returns the number of enemies for a specified wave.
int ds::Level::GetNumberOfWaveEnemies(int wave) {
	return (3 * wave + (wave - 1) * 2);
}