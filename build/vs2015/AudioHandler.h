#ifndef AUDIOHANDLER_H
#define AUDIOHANDLER_H

#include <string>
#include <vector>

#include <platform/vita/audio/audio_manager_vita.h>

namespace ds {

// Struct to hold audio reference data.
// ================
struct SampleRef {
	std::string sampleName;
	Int32 sampleId;
};

// Audio handler class for loading and playing audio samples.
// ================
class AudioHandler {
public:
	AudioHandler(gef::Platform* platform_);
	~AudioHandler();

	SampleRef LoadSample(std::string filename, std::string sampleName);
	void PlaySample(std::string sampleName, bool looping = false);

	bool muted;

private:
	void ReleaseResources();

	gef::Platform* platform;
	gef::AudioManagerVita* audioManager;

	std::vector<SampleRef> sampleLookup;
};

}

#endif