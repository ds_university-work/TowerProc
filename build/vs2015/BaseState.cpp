#include "BaseState.h"

// --CONSTRUCTORS--

ds::state::BaseState::BaseState() {
	initialised = false;
	changeState = false;
	usesMarkers = false;
}

ds::state::BaseState::~BaseState() {
	CleanUpStatePointers();
}

// -----PUBLIC-----

// Links any needed handlers to the state.
void ds::state::BaseState::LinkStatePointers(StatePointers statePointers) {
	platform = statePointers.platform;
	spriteRenderer = statePointers.spriteRenderer;
	renderer3D = statePointers.renderer3D;
	inputManager = statePointers.inputManager;
	audioHandler = statePointers.audioHandler;
	modelHandler = statePointers.modelHandler;
	textureHandler = statePointers.textureHandler;
	debugOverlay = statePointers.debugOverlay;
	arCamera = statePointers.arCamera;

	font = statePointers.font;
}

// ----PROTECTED---

// Releases any used resources.
void ds::state::BaseState::CleanUpStatePointers() {
	delete platform;
	platform = NULL;

	delete spriteRenderer;
	spriteRenderer = NULL;

	delete renderer3D;
	renderer3D = NULL;

	delete inputManager;
	inputManager = NULL;

	delete audioHandler;
	audioHandler = NULL;

	delete modelHandler;
	modelHandler = NULL;

	delete textureHandler;
	textureHandler = NULL;

	delete debugOverlay;
	debugOverlay = NULL;

	delete arCamera;
	arCamera = NULL;

	delete font;
	font = NULL;
}