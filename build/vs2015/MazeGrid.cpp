#include "MazeGrid.h"

ds::procgen::MazeGrid::MazeGrid() {
}

ds::procgen::MazeGrid::~MazeGrid() {
}

// ----------------

// Initialises the maze grid with a size.
void ds::procgen::MazeGrid::SetUpGrid(int x, int y) {
	// Calculates the real grid size.
	xLength = x * 2 - 1;
	yLength = y * 2 - 1;

	// Set up arrays.
	grid = std::vector<std::vector<GenerationTile>>();

	for (int y = 0; y < yLength; y++) {
		grid.push_back(std::vector<GenerationTile>());

		for (int x = 0; x < xLength; x++) {
			grid[y].push_back(GenerationTile(x, y));
		}
	}
}

// Returns the x length of the grid.
int ds::procgen::MazeGrid::GetXLength() {
	return xLength;
}

// Returns the y length of the grid.
int ds::procgen::MazeGrid::GetYLength() {
	return yLength;
}

// Returns a pointer to the given tile.
ds::procgen::GenerationTile* ds::procgen::MazeGrid::GetTile(int x, int y) {
	if (x >= xLength || y >= yLength)
		return NULL;

	return &grid[y][x];
}

// Returns the enemy path from spawner to player home.
std::vector<gef::Vector2> ds::procgen::MazeGrid::GetEnemyPath() {
	std::vector<gef::Vector2> path;
	bool spawnFound = false;
	gef::Vector2 current;

	// Loop the grid tiles.
	for (int y = 0; y < yLength && !spawnFound; y++) {
		for (int x = 0; x < xLength && !spawnFound; x++) {
			// If an enemy spawn is found, set the tile as the current tile.
			if (GetTile(x, y)->type == GenerationTileType::EnemySpawn) {
				current.x = x;
				current.y = y;
				spawnFound = true;

				break;
			}
		}
	}

	bool homeReached = false;

	// Loop while the home has not yet been reached.
	while (!homeReached) {
		// Get a pointer to the current tile.
		GenerationTile* tile = GetTile(current.x, current.y);

		// Add tile to the enemy path.
		path.push_back(tile->gridCoords);

		// If the tile is the player home, then set player home as having been reached.
		if (tile->type == GenerationTileType::PlayerHome) {
			homeReached = true;

			break;
		}

		// Set the current tile's parent as the current tile.
		current = tile->parentTile->gridCoords;
	}

	return path;
}
