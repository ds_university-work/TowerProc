#include "GameplayManager.h"

// --CONSTRUCTORS--

ds::GameplayManager::GameplayManager() {
}

ds::GameplayManager::~GameplayManager() {
}

// -----PUBLIC-----

// Initialises the gameplay manager.
void ds::GameplayManager::Initialise(ModelHandler* modelHandler_, TextureHandler* textureHandler_, AudioHandler* audioHandler_, ARCamera* arCamera_, Level* level) {
	// Link handlers.
	modelHandler = modelHandler_;
	textureHandler = textureHandler_;
	audioHandler = audioHandler_;
	arCamera = arCamera_;
	gameLevel = level;

	// Set gameplay stage.
	gameplayStage = GameplayStage::Build;
	wave = 1;
	alive = true;
	endGame = false;

	// Initialise gameplay values.
	money = 90;
	towerCost = 60;
	health = 10;

	// Initialise player stats.
	gameShotsFired = 0;
	gameEnemiesKilled = 0;
	gameDamageTaken = 0;
	gameTimeTaken = 0;

	// Initialise variables
	waveCountdown = 0;
	waveCompleted = false;
	spawnDelay = 0;

	// Get the number of enemies for the next wave.
	waveEnemyCount = gameLevel->GetNumberOfWaveEnemies(wave);

	// Set up sprites.
	textureHandler->SetUpSprite(&resultScreenBackground,
		"Background",
		gef::Vector4(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2, 0),
		gef::Vector2(DISPLAY_WIDTH * 0.7f, DISPLAY_HEIGHT * 0.8f));
}

// Updates the gameplay manager.
void ds::GameplayManager::Update(float deltaTime) {
	switch (gameplayStage) {
		case GameplayStage::Build:
			break;
		case GameplayStage::Defend:
			// Check if wave countdown has finished.
			if (waveCountdown > 0) {
				// Continue countdown.
				waveCountdown -= deltaTime;
			} else {
				// Update wave time.
				waveTimeTaken += deltaTime;

				// Check if the game should spawn an enemy.
				if (!gameLevel->allEnemiesSpawned) {
					if (spawnDelay <= 0) {
						// Spawn enemy.
						gameLevel->SpawnEnemy();

						// Reset spawn delay.
						spawnDelay = 2.0f - ((float)wave * 0.1f);
						if (spawnDelay < 0.2f) {
							spawnDelay = 0.2f;
						}
					} else {
						// Count down spawn timer.
						spawnDelay -= deltaTime;
					}
				}

				// Update the combat collisions.
				gameLevel->CombatUpdate(&health);

				// Check if the wave has finished.
				if (gameLevel->enemies.size() == 0 && gameLevel->allEnemiesSpawned) {
					waveCompleted = true;
				} else if (health <= 0) {
					waveCompleted = true;
					alive = false;
				}

				// Changes stage if the wave has been completed.
				if (waveCompleted) {
					ChangeStage();
				}
			}

			break;
		case GameplayStage::Result:
			// Do nothing.
			break;
	}

	// Update game objects.
	gameLevel->UpdateObjects(deltaTime);
}

// Renders the 3D aspects of the gameplay manager.
void ds::GameplayManager::Render3D(gef::Renderer3D* renderer3D) {
	// Render game objects if one of the markers can be seen.
	if (arCamera->GetMarker(AR_MARKER_1)->visible || arCamera->GetMarker(AR_MARKER_2)->visible) {
		gameLevel->Render(renderer3D);
	}
}

// Renders the 2D aspects of the gameplay manager.
void ds::GameplayManager::Render2D(gef::SpriteRenderer* spriteRenderer, gef::Font* font) {
	// Get generation stage text.
	std::string stageText = "";

	// Render wave text.
	char waveText[10];
	std::sprintf(waveText, "Wave %01i", wave);
	font->RenderText(spriteRenderer, gef::Vector4(0, 0, 0), 0.75f, 0xffffffff, gef::TextJustification::TJ_LEFT, waveText);

	// Render number of enemies in wave.
	char waveEnemyText[10];
	std::sprintf(waveEnemyText, "Wave enemies: %i", waveEnemyCount);
	font->RenderText(spriteRenderer, gef::Vector4(0, 20, 0), 0.75f, 0xffffffff, gef::TextJustification::TJ_LEFT, waveEnemyText);

	// Render money text.
	char moneyText[10];
	std::sprintf(moneyText, "Money: %i", money);
	font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH, 0, 0), 0.75f, 0xffffffff, gef::TextJustification::TJ_RIGHT, moneyText);

	// Render health text.
	char healthText[10];
	std::sprintf(healthText, "Health: %i", health);
	font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH, 30, 0), 0.75f, 0xffffffff, gef::TextJustification::TJ_RIGHT, healthText);

	switch (gameplayStage) {
		case GameplayStage::Build:
			stageText += "Build Stage";

			char towerCostText[200];
			std::sprintf(towerCostText, "Tower Build Cost: %i", towerCost);
			font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH, DISPLAY_HEIGHT - 30, 0), 0.75f, 0xffffffff, gef::TextJustification::TJ_RIGHT, towerCostText);

			break;
		case GameplayStage::Defend:
			stageText += "Defend Stage";

			if (waveCountdown > 0) {
				char countdown[10];
				std::sprintf(countdown, "%.0f", waveCountdown + 0.5);

				font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2, 0), 2.0f, 0xff0000ff, gef::TextJustification::TJ_CENTRE, countdown);

			}
			break;
		case GameplayStage::Result:
			stageText += "Wave Result";
			resultsTitleText = "RESULTS";
			resultsWaveText = "Wave Stats";
			resultsGameText = "Game Stats";
			if (alive) {
				resultsCompletionText = "Next wave...";
			} else {
				resultsCompletionText = "Game over!";
			}


			// Wave text
			char waveShotsFiredText[30];
			std::sprintf(waveShotsFiredText, "Shots fired:     %i", waveShotsFired);
			char waveEnemiesKilledText[30];
			std::sprintf(waveEnemiesKilledText, "Enemies killed:  %i", waveEnemiesKilled);
			char waveDamageTakenText[30];
			std::sprintf(waveDamageTakenText, "Damage taken:    %i", waveDamageTaken);
			char waveTimeTakenText[30];
			int waveMinutes = waveTimeTaken / 60;
			int waveSeconds = (int)waveTimeTaken % 60;
			std::sprintf(waveTimeTakenText, "Time taken:      %im %is", waveMinutes, waveSeconds);

			// Game text
			char gameShotsFiredText[30];
			std::sprintf(gameShotsFiredText, "Shots Fired:     %i", gameShotsFired);
			char gameEnemiesKilledText[30];
			std::sprintf(gameEnemiesKilledText, "Enemies Killed:  %i", gameEnemiesKilled);
			char gameDamageTakenText[30];
			std::sprintf(gameDamageTakenText, "Damage Taken:    %i", gameDamageTaken);
			char gameTimeTakenText[30];
			int gameMinutes = gameTimeTaken / 60;
			int gameSeconds = (int)gameTimeTaken % 60;
			std::sprintf(gameTimeTakenText, "Time Taken:      %im %is", gameMinutes, gameSeconds);

			// Render
			spriteRenderer->DrawSprite(resultScreenBackground);
			font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH / 2, 50, 0), 1.5f, 0xffffffff, gef::TextJustification::TJ_CENTRE, resultsTitleText.c_str());
			
			font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH * 0.2f, 100, 0), 1.0f, 0xffffffff, gef::TextJustification::TJ_LEFT, resultsWaveText.c_str());
			font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH * 0.25f, 135, 0), 0.7f, 0xffffffff, gef::TextJustification::TJ_LEFT, waveShotsFiredText);
			font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH * 0.25f, 160, 0), 0.7f, 0xffffffff, gef::TextJustification::TJ_LEFT, waveEnemiesKilledText);
			font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH * 0.25f, 185, 0), 0.7f, 0xffffffff, gef::TextJustification::TJ_LEFT, waveDamageTakenText);
			font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH * 0.25f, 210, 0), 0.7f, 0xffffffff, gef::TextJustification::TJ_LEFT, waveTimeTakenText);

			font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH * 0.2f, 250, 0), 1.0f, 0xffffffff, gef::TextJustification::TJ_LEFT, resultsGameText.c_str());
			font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH * 0.25f, 285, 0), 0.7f, 0xffffffff, gef::TextJustification::TJ_LEFT, gameShotsFiredText);
			font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH * 0.25f, 310, 0), 0.7f, 0xffffffff, gef::TextJustification::TJ_LEFT, gameEnemiesKilledText);
			font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH * 0.25f, 335, 0), 0.7f, 0xffffffff, gef::TextJustification::TJ_LEFT, gameDamageTakenText);
			font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH * 0.25f, 360, 0), 0.7f, 0xffffffff, gef::TextJustification::TJ_LEFT, gameTimeTakenText);

			font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH / 2, 450, 0), 1.0f, 0xffffffff, gef::TextJustification::TJ_CENTRE, resultsCompletionText.c_str());

			break;
		default:
			stageText += "stageText overflow";
			break;
	}

	// Render generation stage.
	font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH / 2, 0, 0), 0.75f, 0xffffffff, gef::TextJustification::TJ_CENTRE, stageText.c_str());

}

// Changes the current gameplay stage.
void ds::GameplayManager::ChangeStage() {
	switch (gameplayStage) {
		case GameplayStage::Build:
			// Set enemies to spawn.
			waveEnemyCount = gameLevel->SetEnemiesToSpawn(wave);

			// Reset wave time.
			waveTimeTaken = 0;

			// Start countdown.
			waveCountdown = 3;

			// Change the stage.
			gameplayStage = GameplayStage::Defend;

			break;
		case GameplayStage::Defend:
			// Update stats.
			CalculateResultsStats();

			// Set tower targets.
			for (int i = 0; i < gameLevel->towers.size(); i++) {
				gameLevel->towers[i].SetTarget(NULL);
			}

			// Change the stage.
			gameplayStage = GameplayStage::Result;
			break;
		case GameplayStage::Result:
			if (alive) {
				// Calculate money earned from the last wave.
				CalculateEarnedMoney();

				// Increment the wave.
				waveCompleted = false;
				wave++;

				// Get the number of enemies for the next wave.
				waveEnemyCount = gameLevel->GetNumberOfWaveEnemies(wave);

				// Reset the home health.
				health = 10;

				// Change the stage.
				gameplayStage = GameplayStage::Build;
			} else {
				endGame = true;
			}
			break;
	}
}

// Returns the current gameplay stage.
ds::GameplayStage ds::GameplayManager::GetStage() {
	return gameplayStage;
}

// Places a tower on the grid.
void ds::GameplayManager::PlaceTower(ds::GameObject3D raycast) {
	// Check if raycast object is colliding with a tile.
	for (int y = 0; y < gameLevel->grid.size(); y++) {
		for (int x = 0; x < gameLevel->grid[y].size(); x++) {
			if (ds::utils::CheckCollisionAABB(&raycast, &gameLevel->grid[y][x])) {
				// Check if player has enough money to place tower.
				if (money > towerCost) {
					if (gameLevel->grid[y][x].PlaceTower(modelHandler, audioHandler, &gameLevel->towers, ds::TowerType::Gun, &gameLevel->towerProjectiles)) {
						// Purchase tower.
						money -= towerCost;

						// Increase tower build cost.
						towerCost *= 2;
					}
				}
				return;
			}
		}
	}
}

// -----PRIVATE----

// Calculates the earned money from that wave.
void ds::GameplayManager::CalculateEarnedMoney() {
	int earnedMoney = (5 * waveEnemyCount) * ((float)health / 10);

	money += earnedMoney;
}

// Calculates the player's wave and game stats.
void ds::GameplayManager::CalculateResultsStats() {
	// Shots fired.
	waveShotsFired = 0;
	for (int i = 0; i < gameLevel->towers.size(); i++) {
		waveShotsFired += gameLevel->towers[i].shotsFired;
		gameLevel->towers[i].shotsFired = 0;
	}
	gameShotsFired += waveShotsFired;

	// Enemies killed.
	waveEnemiesKilled = gameLevel->waveEnemiesKilled;
	gameLevel->waveEnemiesKilled = 0;
	gameEnemiesKilled += waveEnemiesKilled;

	// Damage taken.
	waveDamageTaken = 10 - health;
	gameDamageTaken += waveDamageTaken;

	// Time taken.
	gameTimeTaken += waveTimeTaken;
}