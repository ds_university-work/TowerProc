#include "StateHandler.h"

// --CONSTRUCTORS--

ds::state::StateHandler::StateHandler() {
}

ds::state::StateHandler::~StateHandler() {
}

// -----PUBLIC-----

// Initialises the game.
void ds::state::StateHandler::Initialise(gef::Platform* platform_) {
	platform = platform_;

	// Create the sprite renderer.
	spriteRenderer = platform->CreateSpriteRenderer();

	// Create the 3D renderer.
	renderer3D = platform->CreateRenderer3D();

	// Create the input manager.
	inputManager = platform->CreateInputManager();

	// Create the audio handler.
	audioHandler = new ds::AudioHandler(platform);

	// Create the model handler.
	modelHandler = new ds::ModelHandler(platform);

	// Create the texture handler.
	textureHandler = new ds::TextureHandler(platform);

	// Create the debug overlay.
	debugOverlay = new ds::utils::Debug();

	// Create the ar camera.
	arCamera = new ds::ARCamera();
	arCamera->SetUpARCamera(platform);

	// Create the game font.
	font = new gef::Font(*platform_);

	// Load game assets.
	LoadGameAssets();

	// Initialise the state pointer.
	pCurrentState = &splashState;
}

// Updates the game loop.
void ds::state::StateHandler::Update(float deltaTime, AppData* dat) {
	// Calculate the framerate.
	framerate = 1.0f / deltaTime;

	// If current state is to change, then change the state.
	if (pCurrentState->changeState) {
		// Reset state flags.
		pCurrentState->changeState = false;
		pCurrentState->initialised = false;

		// Change pointer to the next state.
		currentStateEnum = pCurrentState->GetNextState();
		switch (currentStateEnum) {
			case GameState::GS_SplashState:
				pCurrentState = &splashState;
				break;
			case GameState::GS_MenuState:
				pCurrentState = &menuState;
				break;
			case GameState::GS_InGameState:
				pCurrentState = &inGameState;
				break;
			case GameState::GS_GameOverState:
				pCurrentState = &gameOverState;
				break;
			case GameState::GS_CreditsState:
				pCurrentState = &creditsState;
				break;
		}
	}

	// If state is to be initialised, then initialise the state.
	if (!pCurrentState->initialised) {
		// Set the pointers to be passed to the state.
		StatePointers sp;
		sp.platform = platform;
		sp.spriteRenderer = spriteRenderer;
		sp.renderer3D = renderer3D;
		sp.inputManager = inputManager;
		sp.audioHandler = audioHandler;
		sp.modelHandler = modelHandler;
		sp.textureHandler = textureHandler;
		sp.debugOverlay = debugOverlay;
		sp.arCamera = arCamera;
		sp.font = font;

		pCurrentState->LinkStatePointers(sp);
		pCurrentState->Initialise();
		pCurrentState->initialised = true;
	}

	// Update the device input manager.
	inputManager->Update();

	// Update AR markers.
	arCamera->UpdateMarkers();

	// Check if the markers have been lost on the screen.
	if (arCamera->GetMarker(AR_MARKER_1)->visible || arCamera->GetMarker(AR_MARKER_2)->visible) {
		markersLost = false;
	} else {
		markersLost = true;
		
		// If the state uses markers, show lost marker message.
		if (pCurrentState->usesMarkers) {
			debugOverlay->AddText("Game Paused: Lost markers 1 and 2", gef::Vector2(270, 240), 0xff0000ff, 1.0f);
		}
	}

	// Get input for debug overlay.
	if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_SELECT) {
		debugOverlay->enabled = !debugOverlay->enabled;
	}

	// Get input for reseting ar camera white balance.
	if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_DOWN) {
		arCamera->ResetCameraWhiteBalance();
	}

	// If NOT(markers lost AND current state uses markers), then update.
	if (!(markersLost && pCurrentState->usesMarkers)) {
		// Update the current game state.
		pCurrentState->Update(deltaTime, dat);
	}
}

// Renders the game to the screen.
void ds::state::StateHandler::Render(AppData* dat) {
	// Render the game to the device screen.
	pCurrentState->Render(dat);

	// Render the debug screen on top of the game screen.
	debugOverlay->Render(spriteRenderer, font);
}

// Releases any used resources.
void ds::state::StateHandler::CleanUp() {
	// Clean up game handlers.
	delete platform;
	platform = NULL;

	delete spriteRenderer;
	spriteRenderer = NULL;

	delete renderer3D;
	renderer3D = NULL;

	delete inputManager;
	inputManager = NULL;

	delete modelHandler;
	modelHandler = NULL;

	// Clean up game fonts.
	delete font;
	font = NULL;

	// Clean up game states.
	delete pCurrentState;
	pCurrentState = NULL;

	splashState.CleanUp();
	menuState.CleanUp();
	inGameState.CleanUp();
	gameOverState.CleanUp();
	creditsState.CleanUp();
}

// -----PRIVATE----

// Loads the game assets.
void ds::state::StateHandler::LoadGameAssets() {
	// Load fonts.
	font->Load("Assets/Fonts/profontwindows");

	// Load 3D models.
	modelHandler->LoadModelScn("cube.scn", "Cube");
	modelHandler->LoadModelScn("Tower_Full.scn", "GunTowerFull");
	modelHandler->LoadModelScn("Tower_Base.scn", "GunTowerBase");
	modelHandler->LoadModelScn("Tower_Top.scn", "GunTowerTop");
	modelHandler->LoadModelScn("Tile_Ground.scn", "TileGround");
	modelHandler->LoadModelScn("Tile_Path.scn", "TilePath");
	modelHandler->LoadModelScn("Enemy.scn", "Enemy");
	modelHandler->LoadModelScn("EnemyObject.scn", "EnemyObject");
	modelHandler->LoadModelScn("HomeObject.scn", "HomeObject");

	// Load 2D textures.
	textureHandler->LoadTexturePNG("Background.png", "Background");
	textureHandler->LoadTexturePNG("DeveloperLogo.png", "DeveloperLogo");
	textureHandler->LoadTexturePNG("GameLogo.png", "GameLogo");
	textureHandler->LoadTexturePNG("MenuButton.png", "MenuButton");
	textureHandler->LoadTexturePNG("MenuHighlight.png", "MenuHighlight");
	textureHandler->LoadTexturePNG("MenuOptionTicked.png", "MenuOptionTicked");
	textureHandler->LoadTexturePNG("MenuOptionUnticked.png", "MenuOptionUnticked");

	// Load game audio.
	audioHandler->LoadSample("gunfire.wav", "Gunfire");
	audioHandler->LoadSample("menuBeep.wav", "MenuBeep");
	audioHandler->LoadSample("airhorn.wav", "Airhorn");
}