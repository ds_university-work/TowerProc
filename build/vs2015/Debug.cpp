#include "Debug.h"

// --CONSTRUCTORS--

ds::utils::Debug::Debug() {
	enabled = true;
}

ds::utils::Debug::~Debug() {
}

// -----PUBLIC-----

// Renders all debug data stored to the debug overlay.
void ds::utils::Debug::Render(gef::SpriteRenderer* spriteRenderer, gef::Font* font) {
	// If the debug overlay is enabled.
	if (enabled) {
		spriteRenderer->Begin(false);

		// Render each stored piece of text.
		for (int i = 0; i < debugText.size(); i++) {
			font->RenderText(spriteRenderer, debugText[i].position, debugText[i].scale, debugText[i].colour, gef::TJ_LEFT, debugText[i].text.c_str());
		}

		spriteRenderer->End();
	}

	debugText.clear();
}

// Add text to be rendered on the debug overlay.
void ds::utils::Debug::AddText(std::string text, gef::Vector2 position, unsigned int colour, float scale) {
	Text t = Text();
	t.text = text;
	t.position = gef::Vector4(position.x, position.y, -0.9f);
	t.colour = colour;
	t.scale = scale;

	debugText.push_back(t);
}