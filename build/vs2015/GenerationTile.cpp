#include "GenerationTile.h"

// --CONSTRUCTORS--

ds::procgen::GenerationTile::GenerationTile(int x, int y) {
	// Set tile grid coordinates.
	gridCoords.x = x;
	gridCoords.y = y;

	parentTile = nullptr;

	// Set the tile as invalid if both x and y coordinates are odd.
	if ((int)gridCoords.x % 2 == 1 && (int)gridCoords.y % 2 == 1) {
		type = GenerationTileType::Invalid;
	} else {
		type = GenerationTileType::Unvisited;
	}
}

ds::procgen::GenerationTile::~GenerationTile() {
}
