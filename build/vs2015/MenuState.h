#ifndef MENUSTATE_H
#define MENUSTATE_H

#include "BaseState.h"

namespace ds {
namespace state {

// Menu screen game state.
// ================
class MenuState : public ds::state::BaseState {
public:
	MenuState();
	~MenuState();

	void Initialise();
	void Update(float deltaTime, AppData* dat);
	void Render(AppData* dat);
	void CleanUp();

	GameState GetNextState();

private:
	void ChangeOption();
	void DoAction();
	void UpdateKonami();
	void CheckKonamiKey(int index, int key);
	void ResetKonami();

	gef::Sprite background;
	gef::Sprite gameLogo;
	gef::Sprite button;
	gef::Sprite audioTick;
	gef::Sprite highlight;

	std::string startText;
	std::string audioMuteText;
	std::string howToPlayText;
	std::vector<std::string> howToPlayDetailsText;
	std::string creditsText;

	bool konami[11];
	bool konamiActive;
	int konamiIndex;

	bool showHowToPlay;
	bool audioMuted;

	ds::state::GameState selectedOption;
	int selectedOptionIndex;

};

}
}

#endif