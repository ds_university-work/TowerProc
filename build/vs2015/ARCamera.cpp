#include "ARCamera.h"

// --CONSTRUCTORS--

ds::ARCamera::ARCamera() {
	// Initialise all markers.
	for (int i = 0; i < 6; i++) {
		markers.push_back(ds::Marker(i));
	}
}

ds::ARCamera::~ARCamera() {
}

// -----PUBLIC-----

// Initialises the augmented reality camera.
void ds::ARCamera::SetUpARCamera(gef::Platform* platform) {
	// Initialise camera feed projection matrix.
	orthoCameraProjectionMatrix = platform->OrthographicFrustum(-1, 1, -1, 1, -1, 1);

	// Calculate camer scaling factor.
	cameraScalingFactor = (960.0f / 640.0f) * (480.0f / 544.0f);

	// Set up camera feed sprite.
	cameraFeed.set_position(gef::Vector4(0, 0, 1));
	cameraFeed.set_width(2.0f);
	cameraFeed.set_height(2.0f * cameraScalingFactor);
	cameraFeed.set_texture(&cameraTexture);

	// Set the scale matrix.
	gef::Matrix44 scaleMatrix;
	scaleMatrix.SetIdentity();
	scaleMatrix.Scale(gef::Vector4(1, cameraScalingFactor, 1));

	// Initialise 3D projection matrix.
	projectionMatrix3D = platform->PerspectiveProjectionFov(SCE_SMART_IMAGE_FOV, ((float)SCE_SMART_IMAGE_WIDTH / (float)SCE_SMART_IMAGE_HEIGHT), 0.01f, 5.0f);
	projectionMatrix3D = projectionMatrix3D * scaleMatrix;

	// Set 3D view matrix to identity matrix.
	viewMatrix3D.SetIdentity();

	gridMarkerVisibilityChange = false;
}

// Updates the status of the AR markers.
void ds::ARCamera::UpdateMarkers() {
	// Get the visibility of markers #1 and #2.
	bool m1 = markers[AR_MARKER_1].visible;
	bool m2 = markers[AR_MARKER_2].visible;

	// Loop for each marker.
	for (int i = 0; i < 6; i++) {
		// If the marker is seen by the camera, store its data.
		if (sampleIsMarkerFound(i)) {
			sampleGetTransform(i, &markers[i].transform);
			markers[i].visible = true;
		} else {
			markers[i].visible = false;
		}
	}

	// Get whether a visibility change has occurred with markers #1 and #2.
	if (markers[AR_MARKER_1].visible != m1 || markers[AR_MARKER_2].visible != m2) {
		gridMarkerVisibilityChange = true;
	} else {
		gridMarkerVisibilityChange = false;
	}
}

// Resets the camera white balance and ISO settings.
void ds::ARCamera::ResetCameraWhiteBalance() {
	sceCameraSetISO(CAMERA_DEVICE, CAMERA_ATTRIBUTE_ISO);
	sceCameraSetWhiteBalance(CAMERA_DEVICE, CAMERA_ATTRIBUTE_WHITEBALANCE);
}

// Renders the AR camera feed to the screen.
void ds::ARCamera::RenderARCameraFeed(AppData* dat, gef::SpriteRenderer* spriteRenderer) {
	// Set projection matrix for camera feed.
	spriteRenderer->set_projection_matrix(orthoCameraProjectionMatrix);

	// Start drawing camera feed.
	spriteRenderer->Begin(true);

	// Draw camera feed.
	if (dat->currentImage) {
		cameraTexture.set_texture(dat->currentImage->tex_yuv);
		spriteRenderer->DrawSprite(cameraFeed);
	}

	// Stop drawing camera feed.
	spriteRenderer->End();
}

// Calibrates the grid marker distances.
void ds::ARCamera::CalibrateGridMarkers() {
	gridMarkerDistance.x = GetMarker(AR_MARKER_2)->transform.GetRow(3).x() - GetMarker(AR_MARKER_1)->transform.GetRow(3).x();
	gridMarkerDistance.y = GetMarker(AR_MARKER_2)->transform.GetRow(3).y() - GetMarker(AR_MARKER_1)->transform.GetRow(3).y();
}

// Returns the data of a given marker index.
ds::Marker* ds::ARCamera::GetMarker(int index) {
	return &markers[index];
}

// Returns the distance between markers #1 and #2.
gef::Vector2 ds::ARCamera::GetGridMarkerDistance() {
	return gridMarkerDistance;
}
