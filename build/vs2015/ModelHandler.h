#ifndef MODELHANDLER_H
#define MODELHANDLER_H

#include <string>
#include <vector>

#include <assets/obj_loader.h>
#include <graphics/mesh.h>
#include <graphics/model.h>
#include <graphics/scene.h>

namespace ds {

// Struct for holding matching Scene and Mesh data.
// ================
struct SceneMesh {
	gef::Scene* scene;
	gef::Mesh* mesh;
};

// Model reference struct for storing obj models in a lookup table.
// ================
struct ModelRef {
	std::string modelName;
	gef::Model* model;
};

// Mesh reference struct for storing scn meshes in a lookup table.
// ================
struct SceneRef {
	std::string meshName;
	SceneMesh sceneMesh;
};

// Model handler class for loading and storing all 3D game models.
// ================
class ModelHandler {
public:
	ModelHandler(gef::Platform* platform_);
	~ModelHandler();

	// Loading
	ModelRef LoadModelObj(std::string filename, std::string modelName);
	SceneRef LoadModelScn(std::string filename, std::string sceneName);

	// Getters
	gef::Mesh* GetMesh(std::string meshName);

private:
	void ReleaseResources();

	gef::Platform* platform;

	std::vector<ModelRef> modelLookup;
	std::vector<SceneRef> sceneLookup;

};

}
#endif