#ifndef LEVEL_H
#define LEVEL_H

#include <vector>

#include <graphics/renderer_3d.h>

#include "ARCamera.h"
#include "Collision.h"
#include "Enemy.h"
#include "GameObject3D.h"
#include "ModelHandler.h"
#include "Projectile.h"
#include "TileGameObject.h"

namespace ds {

// Level class to hold level data and perform certain game object functions.
class Level {
public:
	Level();
	~Level();

	void Initialise(ModelHandler* modelHandler_, ARCamera* arCamera_, gef::Vector4* worldScale_);
	void UpdateObjects(float deltaTime);
	void Render(gef::Renderer3D* renderer3D);

	void SetGrid(std::vector<std::vector<ds::TileGameObject>> levelGrid);
	void SetHome(gef::Vector2 home);
	bool AddRemoveEnemySpawn(gef::Vector2 spawn);
	void UpdateGridAnchoring(bool manual = false);
	int SetEnemiesToSpawn(int wave);
	void SpawnEnemy();
	void CombatUpdate(int* health);
	int GetNumberOfWaveEnemies(int wave);

	std::vector<std::vector<ds::TileGameObject>> grid;
	ds::GameObject3D homeObject;
	std::vector<ds::GameObject3D> enemySpawnerObjects;

	std::vector<ds::Tower> towers;
	std::vector<ds::Projectile> towerProjectiles;
	std::vector<ds::Enemy> enemies;

	gef::Vector2 homeCoordinate;
	std::vector<gef::Vector2> enemySpawnerCoordinates;
	std::vector<gef::Vector2> enemyPath;
	gef::Vector2 gridSize;
	gef::Vector4* worldScale;

	bool allEnemiesSpawned;
	int waveEnemiesKilled;

private:
	ModelHandler* modelHandler;
	ARCamera* arCamera;

	std::vector<EnemyStruct> enemiesToSpawn;
};

}

#endif