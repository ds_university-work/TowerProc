#ifndef LEVELGENERATOR_H
#define LEVELGENERATOR_H

#include <graphics/font.h>
#include <graphics/renderer_3d.h>
#include <graphics/sprite_renderer.h>

#include "ARCamera.h"
#include "Collision.h"
#include "Level.h"
#include "MazeGenerator.h"
#include "ModelHandler.h"

namespace ds {

// Enum for defining the current stage of generation.
// ================
enum GenerationStage {
	SettingGridSize,
	PlacingHomeTile,
	PlacingEnemySpawners,
	ChoosingGeneratedPath,
	FinishedGeneration
};

// Handles the level generation aspect of the game.
// ================
class LevelGenerator {
public:
	LevelGenerator();
	~LevelGenerator();

	void Initialise(ModelHandler* modelHandler_, ARCamera* arCamera_, Level* level, gef::Vector4* worldScale_);
	void UpdateStage(float deltaTime);
	void DoStageAction();
	void NextStage();
	void PreviousStage();
	ds::GenerationStage GetStage();

	void Render3D(gef::Renderer3D* renderer3D);
	void Render2D(gef::SpriteRenderer* spriteRenderer, gef::Font* font);

	bool finishedGeneration;

private:
	void UpdateGridSize();
	void PlaceHomeTile();
	void AddRemoveEnemySpawner();
	void GeneratePath();

	ModelHandler* modelHandler;
	ARCamera* arCamera;
	Level* gameLevel;

	GenerationStage generationStage;

	bool homePlaced;
	bool enemySpawnPlaced;
	bool pathGenerated;

	ds::GameObject3D homeIndicator;
	ds::GameObject3D enemySpawnIndicator;

	procgen::MazeGenerator mazeGenerator;
	procgen::MazeGrid mazeGrid;

	float tileSeparation;
	gef::Vector4* worldScale;

};

}

#endif