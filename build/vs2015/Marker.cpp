#include "Marker.h"

// --CONSTRUCTORS--

ds::Marker::Marker(int index) {
	markerIndex = index;
	visible = false;
	transform.SetIdentity();
}

ds::Marker::~Marker() {
}