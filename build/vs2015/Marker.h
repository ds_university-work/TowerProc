#ifndef MARKER_H
#define MARKER_H

#include <maths/matrix44.h>

namespace ds {

// Class for representing the physical markers.
// =================
class Marker {
public:
	Marker(int index);
	~Marker();
	
	int markerIndex;
	bool visible;
	gef::Matrix44 transform;
};

}

#endif