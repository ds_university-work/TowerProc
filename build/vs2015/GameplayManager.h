#ifndef GAMEPLAYMANAGER_H
#define GAMEPLAYMANAGER_H

#include <graphics/font.h>
#include <graphics/renderer_3d.h>
#include <graphics/sprite_renderer.h>

#include "ARCamera.h"
#include "Collision.h"
#include "Level.h"
#include "ModelHandler.h"
#include "TextureHandler.h"

namespace ds {

// Enum for defining the current gameplay stage.
// ================
enum GameplayStage {
	Build,
	Defend,
	Result
};

// Class for handling all of the gameplay of the game.
// ================
class GameplayManager {
public:
	GameplayManager();
	~GameplayManager();

	void Initialise(ModelHandler* modelHandler_, TextureHandler* textureHandler_, AudioHandler* audioHandler_, ARCamera* arCamera_, Level* level);
	void Update(float deltaTime);
	void Render3D(gef::Renderer3D* renderer3D);
	void Render2D(gef::SpriteRenderer* spriteRenderer, gef::Font* font);

	void ChangeStage();
	GameplayStage GetStage();

	void PlaceTower(ds::GameObject3D raycast);

	bool alive;
	bool endGame;

private:
	void CalculateEarnedMoney();
	void CalculateResultsStats();;

	ModelHandler* modelHandler;
	TextureHandler* textureHandler;
	AudioHandler* audioHandler;
	ARCamera* arCamera;

	Level* gameLevel;

	GameplayStage gameplayStage;
	int wave;

	int money;
	int towerCost;
	int health;
	int waveEnemyCount;

	float waveCountdown;
	bool waveCompleted;
	float spawnDelay;

	int waveShotsFired;
	int waveEnemiesKilled;
	int waveDamageTaken;
	float waveTimeTaken;
	int gameShotsFired;
	int gameEnemiesKilled;
	int gameDamageTaken;
	float gameTimeTaken;
	
	gef::Sprite resultScreenBackground;
	std::string resultsTitleText;
	std::string resultsWaveText;
	std::string resultsGameText;
	std::string resultsCompletionText;

};

}

#endif