#ifndef TILEGAMEOBJECT_H
#define TILEGAMEOBJECT_H

#include "GameObject3D.h"


#include "AudioHandler.h"
#include "GenerationTile.h"
#include "ModelHandler.h"
#include "Tower.h"

namespace ds {

// Forward declarations
enum TowerType;
class Tower;
class Projectile;

// Enum to define the type of tile.
// ===============
enum TileType {
	Ground,
	EnemyPath,
	Home,
	Spawn,
};

// Game object class for 3D grid tiles.
// ================
class TileGameObject : public GameObject3D {
public:
	TileGameObject();
	TileGameObject(gef::Mesh* mesh, gef::Matrix44* anchor, gef::Vector4 position, gef::Vector4* worldScale);
	~TileGameObject();

	void Initialise(gef::Vector2 gridCoordinates, TileType tileType);
	bool PlaceTower(ModelHandler* modelHandler, ds::AudioHandler* audioHandler, std::vector<Tower>* towers, TowerType towerType, std::vector<Projectile>* projectiles);
	void SetTileType(TileType t);

	// Getters
	gef::Vector2 GetGridCoords();
	TileType GetTileType();
	GameObject3D* GetTower();

private:
	void OverrideableUpdate(float deltaTime);
	bool CanPlaceTower();

	gef::Vector2 gridCoords;
	TileType type;
	GameObject3D* installedTower;

};

}

#endif