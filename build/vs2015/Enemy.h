#ifndef ENEMY_H
#define ENEMY_H

#include "GameObject3D.h"
#include "TileGameObject.h"

namespace ds {

// Forward declarations
class TileGameObject;

// Struct for storing enemy data.
// ================
struct EnemyStruct {
	float speed;
	float health;
	TileGameObject* spawnTile;
	std::vector<ds::TileGameObject*> path;
};

// Game object class for representing an enemy.
// ================
class Enemy : public GameObject3D {
public:
	Enemy();
	Enemy(gef::Mesh* mesh, gef::Matrix44* anchor, gef::Vector4 positio, gef::Vector4* worldScalen);
	~Enemy();

	void Initialise(std::vector<ds::TileGameObject*> path, float speed, float healthPoints);
	void UpdateStartPosition();

	float health;
	bool destroy;

private:
	void OverrideableUpdate(float deltaTime);

	gef::Vector4 startPosition;
	std::vector<ds::TileGameObject*> waypoints;
	int waypointIndex;

	float movementSpeed;
	float timePassed;
};

}

#endif