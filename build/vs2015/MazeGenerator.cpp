#include "MazeGenerator.h"

// --CONSTRUCTORS--

ds::procgen::MazeGenerator::MazeGenerator() {
	// Set up pseudorandom number generator seed.
	std::srand(time(NULL));
}

ds::procgen::MazeGenerator::~MazeGenerator() {
}

// -----PUBLIC-----

// Generates a grid maze and returns it.
ds::procgen::MazeGrid ds::procgen::MazeGenerator::GenerateMaze(gef::Vector2 gridSize, gef::Vector2 home, std::vector<gef::Vector2> enemySpawns) {
	endReached = false;

	// Initialise grid.
	grid.SetUpGrid(gridSize.x, gridSize.y);

	// Set home tile.
	grid.GetTile(home.x, home.y)->type = GenerationTileType::PlayerHome;

	// Set enemy spawn tiles.
	for (int i = 0; i < enemySpawns.size(); i++) {
		grid.GetTile(enemySpawns[i].x, enemySpawns[i].y)->type = GenerationTileType::EnemySpawn;
	}

	// ======================
	// Generation starts here
	// ======================

	// Start genertion with home tile.
	current = home;

	// Generate maze.
	while (!endReached) {
		WalkPhase();
		HuntPhase();
	}

	// Get pathing for enemy spawns.
	// TODO: Expand this for multiple enemy spawns. -Dean
	CalculateEnemyPaths(enemySpawns);

	return grid;
}

// ----PRIVATE-----

// Performs walk phase, where the generator finds adjacent unvisited tiles.
void ds::procgen::MazeGenerator::WalkPhase() {
	bool walk = true;

	// Loop while there is a valid adjacent tile.
	while (walk) {
		// Check whether there is a valid adjacent tile, and the direction of it.
		ValidDirection vd = GetValidRandomDirection(GenerationTileType::Unvisited);

		if (vd.valid) {
			// Get the coordinates of the adjacent tile.
			gef::Vector2 adjTile = gef::Vector2(current.x + GetDirectionValueX(vd.direction),
												current.y + GetDirectionValueY(vd.direction));

			// Set the parent of the adjacent tile as the current tile.
			grid.GetTile(adjTile.x, adjTile.y)->parentTile = grid.GetTile(current.x, current.y);

			// Set the current tile type as visited, unless it is the player home.
			if (grid.GetTile(current.x, current.y)->type != GenerationTileType::PlayerHome) {
				grid.GetTile(current.x, current.y)->type = GenerationTileType::Visited;
			}

			// Set the current tile as the adjacent tile.
			current = adjTile;
		} else {
			// There are no valid adjacent tiles.
			walk = false;
		}
	}
}

// Performs hunt phase, where the generator looks for an unvisited tile.
void ds::procgen::MazeGenerator::HuntPhase() {
	// Loop through the grid tiles.
	for (int y = 0; y < grid.GetYLength(); y++) {
		for (int x = 0; x < grid.GetXLength(); x++) {
			// If tile is unvisited.
			if (grid.GetTile(x, y)->type == GenerationTileType::Unvisited) {
				// Set the current tile.
				current = gef::Vector2(x, y);

				// Check whether there is a valid adjacent tile, and the direction of it.
				ValidDirection vd = GetValidRandomDirection(GenerationTileType::Visited);

				if (vd.valid) {
					// Get the coordinates of the adjacent tile.
					gef::Vector2 adjTile = gef::Vector2(current.x + GetDirectionValueX(vd.direction),
														current.y + GetDirectionValueY(vd.direction));

					// Set the parent of the current tile as the adjacent tile.
					grid.GetTile(current.x, current.y)->parentTile = grid.GetTile(adjTile.x, adjTile.y);

					// Set the current tile type as visited
					grid.GetTile(current.x, current.y)->type = GenerationTileType::Visited;

					// Set the current tile as the adjacent tile.
					current = adjTile;

					// Return to the walk phase.
					return;
				}
			}
		}
	}

	// There are no unvisited tiles left.
	endReached = true;
}

// Calculates the enemy paths from the spawn to the player home.
void ds::procgen::MazeGenerator::CalculateEnemyPaths(std::vector<gef::Vector2> enemySpawns) {
	// Loop for each enemy spawn.
	for (int i = 0; i < enemySpawns.size(); i++) {
		current = enemySpawns[i];

		// Check whether there is a valid adjacent tile, and the direction of it.
		ValidDirection vd = GetValidRandomDirection(GenerationTileType::Visited);

		if (vd.valid) {
			// Get the coordinates of the adjacent tile.
			gef::Vector2 newTile = gef::Vector2(current.x + GetDirectionValueX(vd.direction),
												current.y + GetDirectionValueY(vd.direction));

			// Set the parent of the adjacent tile as the current tile.
			grid.GetTile(current.x, current.y)->parentTile = grid.GetTile(newTile.x, newTile.y);

			// Set the current tile as the adjacent tile.
			current = newTile;
		}

		bool homeReached = false;

		// Loop while the player home has not yet been reached by the path.
		while (!homeReached) {
			// If the current tile is the player home.
			if (grid.GetTile(current.x, current.y)->type == GenerationTileType::PlayerHome) {
				homeReached = true;

				continue;
			}

			// Set the current tile type as part of the enemy path.
			grid.GetTile(current.x, current.y)->type = GenerationTileType::Path;

			// Set the current tile's parent as the current tile.
			current = grid.GetTile(current.x, current.y)->parentTile->gridCoords;
		}
	}
}

// Returns a random valid direction, or returns if there are no valid directions.
ds::procgen::ValidDirection ds::procgen::MazeGenerator::GetValidRandomDirection(ds::procgen::GenerationTileType type) {
	Direction next;

	// Get random direction.
	int index = rand() % 4;

	// Loop for each direction.
	for (int i = 0; i < 4; i++) {
		next = (Direction)index;

		// If the adjacent tile is valid, return that direction.
		if (IsDirectionValid(next, type)) {
			return ValidDirection(next);
		} else {
			// Choose next direction.
			index = (index + 1) % 4;
		}
	}

	// There are no valid directions.
	return ValidDirection();
}

// Checks whether a direction is valid.
bool ds::procgen::MazeGenerator::IsDirectionValid(ds::procgen::Direction d, ds::procgen::GenerationTileType type) {
	// Get the adjacent tile's coordinates.
	gef::Vector2 newTile = gef::Vector2(current.x + GetDirectionValueX(d), current.y + GetDirectionValueY(d));

	// Check if the tile is within the grid bounds.
	if (newTile.y < 0)
		return false;
	if (newTile.x < 0)
		return false;
	if (newTile.y >= grid.GetYLength())
		return false;
	if (newTile.x >= grid.GetXLength())
		return false;

	// Check if the tile is of the correct type.
	if (grid.GetTile(newTile.x, newTile.y)->type != type)
		return false;

	// Direction is valid.
	return true;
}

// Returns the relative x value of a given direction.
int ds::procgen::MazeGenerator::GetDirectionValueX(ds::procgen::Direction d) {
	switch (d) {
		case Direction::North:
		case Direction::South:
			return 0;
		case Direction::East:
			return 1;
		case Direction::West:
			return -1;
	}
}

// Returns the relative y value of a given direction.
int ds::procgen::MazeGenerator::GetDirectionValueY(ds::procgen::Direction d) {
	switch (d) {
		case Direction::North:
			return -1;
		case Direction::South:
			return 1;
		case Direction::East:
		case Direction::West:
			return 0;
	}
}