#ifndef INGAMESTATE_H
#define INGAMESTATE_H

#include "BaseState.h"

#include "graphics/sprite.h"
#include "maths/math_utils.h"
#include "platform/vita/graphics/texture_vita.h"

#include "ARCamera.h"
#include "Collision.h"
#include "GameObject3D.h"
#include "GameplayManager.h"
#include "Level.h"
#include "LevelGenerator.h"
#include "Marker.h"
#include "MazeGenerator.h"

namespace ds {
namespace state {

// In game screen game state.
// ================
class InGameState : public ds::state::BaseState {
public:
	InGameState();
	~InGameState();

	void Initialise();
	void Update(float deltaTime, AppData* dat);
	void Render(AppData* dat);
	void CleanUp();

	GameState GetNextState();

private:
	void Render3D();
	void Render2D();

	void LevelGenerationUpdate(float deltaTime);
	void GameplayUpdate(float deltaTime);
	void UpdateHelpScreen();

	// Handlers
	LevelGenerator levelGenerator;
	GameplayManager gameplayManager;
	Level gameLevel;

	// Game Objects
	ds::GameObject3D raycast;

	// HUD Objects
	std::string helpTitleText;
	std::vector<std::string> helpText;

	bool showHelpScreen;
	gef::Sprite helpWindowBackground;

	gef::Vector4 worldScale;
};

}
}

#endif