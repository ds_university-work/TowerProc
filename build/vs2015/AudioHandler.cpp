#include "AudioHandler.h"

// --CONSTRUCTORS--

ds::AudioHandler::AudioHandler(gef::Platform* platform_) {
	sampleLookup = std::vector<SampleRef>();
	platform = platform_;
	audioManager = new gef::AudioManagerVita();

	muted = false;
}

ds::AudioHandler::~AudioHandler() {
	ReleaseResources();
}

// -----PUBLIC-----

// Loads in an audio sample from a wav file.
ds::SampleRef ds::AudioHandler::LoadSample(std::string filename, std::string sampleName) {
	// Create new element in the sample lookup to store the loaded sample.
	sampleLookup.push_back(SampleRef());

	std::string filepath = "Assets/Audio/" + filename;

	// Load the sample into the audio manager.
	Int32 sample = audioManager->LoadSample(filepath.c_str(), *platform);
	
	// If the sample was loaded successfully, add the sample to the lookup table.
	if (sample >= 0) {
		sampleLookup.back().sampleId = sample;
		sampleLookup.back().sampleName = sampleName;

		return sampleLookup.back();
	} else {
		sampleLookup.pop_back();
	}
	
	return ds::SampleRef();
}

// Plays an audio sample from a given name.
void ds::AudioHandler::PlaySample(std::string sampleName, bool looping) {
	// If the audio is not muted.
	if (!muted) {
		Int32 sample = -1;

		// Loops through the sample lookup to find a matching sample.
		for (int i = 0; i < sampleLookup.size(); i++) {
			// If the sample is found, store its ID.
			if (sampleName == sampleLookup[i].sampleName) {
				sample = sampleLookup[i].sampleId;
				break;
			}
		}

		// If a sample was found, play it.
		if (sample != -1) {
			audioManager->PlaySample(sample, looping);
		}
	}
}

// -----PRIVATE----

// Releases any used resources.
void ds::AudioHandler::ReleaseResources() {
	delete platform;
	platform = NULL;

	delete audioManager;
	audioManager = NULL;
}