#include "TextureHandler.h"

// --CONSTRUCTORS--

ds::TextureHandler::TextureHandler(gef::Platform* platform_) {
	textureLookup = std::vector<TextureRef>();
	platform = platform_;
}

ds::TextureHandler::~TextureHandler() {
	ReleaseResources();
}

// -----PUBLIC-----

// Loads in a texture from a png.
ds::TextureRef ds::TextureHandler::LoadTexturePNG(std::string filename, std::string textureName) {
	// Create new element in the texture lookup to store the loaded texture.
	textureLookup.push_back(TextureRef());

	// Create object for image data.
	gef::ImageData imageData;

	std::string filepath = "Assets/Textures/" + filename;


	// Load .png texture.
	pngLoader.Load(filepath.c_str(), *platform, imageData);
	
	// Add TextureRef data to the lookup table.
	textureLookup.back().texture = gef::Texture::Create(*platform, imageData);
	textureLookup.back().textureName = textureName;

	return textureLookup.back();
}

// Returns a preloaded texture from the lookup table.
gef::Texture* ds::TextureHandler::GetTexture(std::string textureName) {
	// Check the lookup table.
	for (int i = 0; i < textureLookup.size(); i++) {
		if (textureLookup[i].textureName == textureName) {
			return textureLookup[i].texture;
		}
	}

	// Texture does not exist.
	return NULL;
}

// Sets up a sprite.
void ds::TextureHandler::SetUpSprite(gef::Sprite* sprite, std::string textureName, gef::Vector4 position, gef::Vector2 dimensions) {
	sprite->set_texture(GetTexture(textureName));
	sprite->set_position(position);
	sprite->set_width(dimensions.x);
	sprite->set_height(dimensions.y);
}

// -----PRIVATE----

// Releases any used resources.
void ds::TextureHandler::ReleaseResources() {
	// Release the stored textures.
	for (int i = 0; i < textureLookup.size(); i++) {
		delete textureLookup[i].texture;
	}
	textureLookup.clear();
}