#include "GameOverState.h"

// ===============
//   DEPRECRATED
// ===============

ds::state::GameOverState::GameOverState() {
}

ds::state::GameOverState::~GameOverState() {
}

void ds::state::GameOverState::Initialise() {
}

void ds::state::GameOverState::Update(float deltaTime, AppData* dat) {
}

void ds::state::GameOverState::Render(AppData* dat) {
}

void ds::state::GameOverState::CleanUp() {
}

ds::state::GameState ds::state::GameOverState::GetNextState() {
	return GameState();
}
