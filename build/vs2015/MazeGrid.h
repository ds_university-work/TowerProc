#ifndef MAZEGRID_H
#define MAZEGRID_H

#include <vector>

#include "GenerationTile.h"

namespace ds {
namespace procgen {

class MazeGrid {
public:
	MazeGrid();
	~MazeGrid();

	// Initialisation
	void SetUpGrid(int x, int y);

	// Getters
	int GetXLength();
	int GetYLength();
	GenerationTile* GetTile(int x, int y);
	std::vector<gef::Vector2> GetEnemyPath();

	std::vector<std::vector<GenerationTile>> grid;

private:
	int xLength;
	int yLength;

};

}
}
#endif