#ifndef GAMEOBJECT3D_H
#define GAMEOBJECT3D_H

// Includes
#include <graphics/mesh.h>
#include <graphics/mesh_instance.h>
#include <maths/quaternion.h>

namespace ds {

// GameObject class for 3D meshes.
// ================
class GameObject3D : public gef::MeshInstance {
public:
	GameObject3D();
	GameObject3D(gef::Mesh* mesh, gef::Matrix44* anchor, gef::Vector4 position, gef::Vector4* worldScale_, bool anchorScale = true);
	virtual ~GameObject3D();

	void Update(float deltaTime);

	// Position
	void SetPosition(gef::Vector4 position);
	void AdjustPosition(gef::Vector4 positionDelta);

	// Rotation
	void SetRotationX(float degrees);
	void SetRotationY(float degrees);
	void SetRotationZ(float degrees);
	void SetRotation(gef::Vector4 rotation);
	void SetRotationMatrix(gef::Matrix44 rotation);

	// Scaling
	bool SetScale(gef::Vector4 scaling);
	bool SetScale(float scale);

	// World Anchor
	void SetWorldAnchor(gef::Matrix44* anchor, bool anchorScale = true);

	// Getters
	gef::Mesh GetMesh();
	gef::Vector4 GetPosition(bool local = true);
	gef::Matrix44* GetWorldTransform();
	gef::Vector4 GetLocalScale();
	
	gef::Matrix44 LocalTransform;
	gef::Matrix44* AnchorTransform;

	bool useAnchorScale;

protected:
	gef::Vector4* worldScale;

private:
	void UpdateWorldTransform();
	void UpdateLocalTransform();

	virtual void OverrideableUpdate(float deltaTime);

	gef::Vector4 LocalPosition;
	gef::Matrix44 LocalRotation;
	gef::Matrix44 LocalScale;
};

}
#endif
