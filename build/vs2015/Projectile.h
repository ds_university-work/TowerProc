#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "Collision.h"
#include "Enemy.h"
#include "GameObject3D.h"

namespace ds {

// Forward delcarations
class Enemy;

// Game object class to represent a fired projectile.
class Projectile : public ds::GameObject3D {
public:
	Projectile();
	Projectile(gef::Mesh* mesh, gef::Matrix44* anchor, gef::Vector4 position, gef::Vector4* worldScale);
	~Projectile();

	void Initialise(ds::Enemy* enemyTarget, gef::Vector4 start, gef::Vector4 end, float speed, float damage_);
	void DamageEnemy();

	bool destroy;

private:
	void OverrideableUpdate(float deltaTime);

	ds::Enemy* target;

	gef::Vector4 startPosition;
	gef::Vector4 endPosition;

	float movementSpeed;
	float damage;
	float lifetime;
};

}

#endif