#ifndef CAMERA_H
#define CAMERA_H

#include <maths/matrix44.h>
#include <maths/vector4.h>
#include <system/platform.h>

namespace ds {

// Camera viewport class.
// ================
class Camera {
public:
	Camera();
	~Camera();

	// Initialisation
	void SetUpCamera(gef::Vector4 position, gef::Vector4 lookAt, gef::Vector4 up, float fov, float near, float far);

	// Position
	void SetPosition(gef::Vector4 position);
	void AdjustPosition(gef::Vector4 positionDelta);
	void PanCamera(gef::Vector4 delta);

	// Look At
	void SetLookAt(gef::Vector4 lookAt);
	void AdjustLookAt(gef::Vector4 lookAtDelta);

	// Up
	void SetUpVector(gef::Vector4 up);

	// Miscellaneous
	void SetFOV(float fov);
	void SetNearPlane(float near);
	void SetFarPlane(float far);

	// Getters
	gef::Vector4 GetPosition();
	gef::Vector4 GetLookAt();
	gef::Vector4 GetUpVector();
	float GetFOV();
	float GetNearPlane();
	float GetFarPlane();
	gef::Matrix44 GetProjectionMatrix(gef::Platform* platform);
	gef::Matrix44 GetViewMatrix();

private:
	gef::Vector4 positionVector;
	gef::Vector4 lookAtVector;
	gef::Vector4 upVector;

	float fieldOfView;
	float nearPlane;
	float farPlane;
};

}
#endif