#ifndef SPLASHSTATE_H
#define SPLASHSTATE_H

#include <graphics/sprite.h>

#include "BaseState.h"

namespace ds {
namespace state {

// Splash screen game state.
// ================
class SplashState : public ds::state::BaseState {
public:
	SplashState();
	~SplashState();

	void Initialise();
	void Update(float deltaTime, AppData* dat);
	void Render(AppData* dat);
	void CleanUp();

	GameState GetNextState();

private:
	gef::Sprite background;
	gef::Sprite devLogo;
	std::string yearText;

	float timePassed;

};

}
}

#endif