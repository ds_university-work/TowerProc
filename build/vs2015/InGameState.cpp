#include "InGameState.h"
#include <stdio.h>

// --CONSTRUCTORS--

ds::state::InGameState::InGameState() {
}

ds::state::InGameState::~InGameState() {
}

// -----PUBLIC-----

// Initialises the in game state.
void ds::state::InGameState::Initialise() {
	// Initialise the world scale.
	worldScale = gef::Vector4(0.01f, 0.01f, 0.01f);
	
	// Initialise the game level.
	gameLevel = Level();
	gameLevel.Initialise(modelHandler, arCamera, &worldScale);

	// Initialise the level generator.
	levelGenerator = LevelGenerator();
	levelGenerator.Initialise(modelHandler, arCamera, &gameLevel, &worldScale);

	// Initialise the gameplay manager.
	gameplayManager = GameplayManager();
	gameplayManager.Initialise(modelHandler, textureHandler, audioHandler, arCamera, &gameLevel);

	// Set up sprites.
	textureHandler->SetUpSprite(&helpWindowBackground,
		"Background",
		gef::Vector4(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2, 0),
		gef::Vector2(0, 0));

	// Set up lights.
	gef::PointLight leftPointLight;
	leftPointLight.set_colour(gef::Colour(0.7f, 0.7f, 1.0f, 0.5f));
	leftPointLight.set_position(gef::Vector4(-300.0f, 100.0f, 10.0f));

	gef::PointLight rightPointLight;
	rightPointLight.set_colour(gef::Colour(0.3f, 0.0f, 0.0f, 0.5f));
	rightPointLight.set_position(gef::Vector4(300.0f, 100.0f, 0.0f));

	gef::Default3DShaderData& defaultShaderData = renderer3D->default_shader_data();
	defaultShaderData.set_ambient_light_colour(gef::Colour(0.5f, 0.5f, 0.5f, 1.0f));
	defaultShaderData.AddPointLight(leftPointLight);
	defaultShaderData.AddPointLight(rightPointLight);

	// Initialise game objects.
	raycast = GameObject3D(modelHandler->GetMesh("Cube"), NULL, gef::Vector4(0, 0, 0), &worldScale);
	raycast.SetScale(gef::Vector4(0.002f, 0.002f, 75));

	// Initialise variables.
	usesMarkers = true;
	showHelpScreen = false;
}

// Updates the in game state.
void ds::state::InGameState::Update(float deltaTime, AppData* dat) {

	// Update the anchoring of the grid.
	if (levelGenerator.finishedGeneration || levelGenerator.GetStage() != ds::GenerationStage::SettingGridSize) {
		gameLevel.UpdateGridAnchoring();
	}

	// Recalibrate the grid marker distance.
	if (inputManager->controller_input()->GetController(0)->buttons_down() == gef_SONY_CTRL_UP) {
		arCamera->CalibrateGridMarkers();
		gameLevel.UpdateGridAnchoring(true);
	}

	// Toggle help screen.
	if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_TRIANGLE) {
		showHelpScreen = true;
	}
	if (inputManager->controller_input()->GetController(0)->buttons_released() == gef_SONY_CTRL_TRIANGLE) {
		showHelpScreen = false;
	}

	if (!levelGenerator.finishedGeneration) {
		// Level generation updates
		LevelGenerationUpdate(deltaTime);
	} else {
		// Gameplay updates
		GameplayUpdate(deltaTime);
	}
}

// Renders the in game state.
void ds::state::InGameState::Render(AppData* dat) {
	// Render the ar camera feed.
	arCamera->RenderARCameraFeed(dat, spriteRenderer);

	// Render the 3D game scene.
	Render3D();

	// Render the 2D game overlay.
	Render2D();
}

// Releases any used resources.
void ds::state::InGameState::CleanUp() {
}

// Returns the state to change to.
ds::state::GameState ds::state::InGameState::GetNextState() {
	return GameState::GS_MenuState;
}

// -----PRIVATE----

// Renders the 3D aspects of the in game state.
void ds::state::InGameState::Render3D() {
	// Set up the camera for rendering 3D.
	renderer3D->set_projection_matrix(arCamera->projectionMatrix3D);
	renderer3D->set_view_matrix(arCamera->viewMatrix3D);

	// Start drawing 3D meshes.
	renderer3D->Begin(false);

	// Render the raycast object.
	renderer3D->DrawMesh(raycast);

	// Render level generator objects.
	if (!levelGenerator.finishedGeneration) {
		levelGenerator.Render3D(renderer3D);
	}
	// Render gameplay objects.
	if (levelGenerator.finishedGeneration) {
		gameplayManager.Render3D(renderer3D);
	}

	// Stop drawing 3D meshes.
	renderer3D->End();
}

// Renders the 2D aspects of the in game state.
void ds::state::InGameState::Render2D() {
	// Set up the projection matrix for 2D overlay.
	gef::Matrix44 projectionMatrix2D = platform->OrthographicFrustum(0.0f, platform->width(), 0.0f, platform->height(), -1.0f, 1.0f);
	spriteRenderer->set_projection_matrix(projectionMatrix2D);

	// Start drawing 2D sprite overlay.
	spriteRenderer->Begin(false);

	// Check font exists.
	if (!font) {
		return;
	}

	// Render level generator UI.
	if (!levelGenerator.finishedGeneration) {
		levelGenerator.Render2D(spriteRenderer, font);
	}

	// Render gameplay UI.
	if (levelGenerator.finishedGeneration) {
		gameplayManager.Render2D(spriteRenderer, font);
	}

	// Render help screen.
	if (showHelpScreen) {
		UpdateHelpScreen();
	}

	// Stop drawing 2D sprites.
	spriteRenderer->End();
}

// Updates the level generator.
void ds::state::InGameState::LevelGenerationUpdate(float deltaTime) {
	// Move to the next stage of generation.
	if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_R2) {
		levelGenerator.NextStage();
		audioHandler->PlaySample("MenuBeep");
	}

	// Move to the previous stage of generation.
	if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_L2) {
		levelGenerator.PreviousStage();
		audioHandler->PlaySample("MenuBeep");
	}

	// Perform level generation action.
	if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_CROSS) {
		levelGenerator.DoStageAction();
	}

	// Update level generation.
	levelGenerator.UpdateStage(deltaTime);
}

// Updates the gameplay manager.
void ds::state::InGameState::GameplayUpdate(float deltaTime) {
	switch (gameplayManager.GetStage()) {
		case GameplayStage::Build:
			// Place tower.
			if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_SQUARE) {
				gameplayManager.PlaceTower(raycast);
			}

			// Start wave.
			if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_CROSS) {
				gameplayManager.ChangeStage();
			}
			break;
		case GameplayStage::Defend:
			// Do nothing.
			break;
		case GameplayStage::Result:
			// Change game stage.
			if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_CROSS) {
				gameplayManager.ChangeStage();
			}

			// Quit the game.
			if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_CIRCLE) {
				changeState = true;
			}

			if (gameplayManager.endGame) {
				changeState = true;
			}
			break;
	}

	// Update the gameplay.
	gameplayManager.Update(deltaTime);
}

// Updates the help screen.
void ds::state::InGameState::UpdateHelpScreen() {
	// Clear the help text array.
	helpText.clear();

	// While generating the level.
	if (!levelGenerator.finishedGeneration) {
		switch (levelGenerator.GetStage()) {
			case GenerationStage::SettingGridSize:
				helpText.push_back("Use markers #1 and #2 to set the size of the grid.");
				helpText.push_back("");
				helpText.push_back("Press [R] to go to the next stage.");
				helpText.push_back("Press [Down] to reset the camera white balance.");

				helpWindowBackground.set_width(DISPLAY_WIDTH * 0.6f);
				break;
			case GenerationStage::PlacingHomeTile:
				helpText.push_back("Using marker #3 as a reference, place the home tile on the grid.");
				helpText.push_back("This home will be the tile in which the enemy shall attack.");
				helpText.push_back("");
				helpText.push_back("Press [CROSS] to set the home tile.");
				helpText.push_back("Press [R] to go to the next stage.");
				helpText.push_back("Press [L] to go to the previous stage.");
				helpText.push_back("Press [Down] to reset the camera white balance.");
				helpText.push_back("Press [Up] to calibrate the grid anchoring");
				helpText.push_back("(do this while looking directly down on the markers).");

				helpWindowBackground.set_width(DISPLAY_WIDTH * 0.75f);
				break;
			case GenerationStage::PlacingEnemySpawners:
				helpText.push_back("Using marker #4 as a reference, place enemy spawners* on the grid.");
				helpText.push_back("This is where enemies will spawn from, and will navigate to the home tile from.");
				helpText.push_back("");
				helpText.push_back("*Currently level generation only works with a single spawner.");
				helpText.push_back("");
				helpText.push_back("Press [CROSS] to add/remove an enemy spawn.");
				helpText.push_back("Press [R] to go to the next stage.");
				helpText.push_back("Press [L] to go to the previous stage.");
				helpText.push_back("Press [Down] to reset the camera white balance.");
				helpText.push_back("Press [Up] to calibrate the grid anchoring");
				helpText.push_back("(do this while looking directly down on the markers).");

				helpWindowBackground.set_width(DISPLAY_WIDTH * 0.9f);
				break;
			case GenerationStage::ChoosingGeneratedPath:
				helpText.push_back("Choose a generation path which the enemies will follow along.");
				helpText.push_back("");
				helpText.push_back("Press [CROSS] to generate a new path.");
				helpText.push_back("Press [R] to go to the next stage.");
				helpText.push_back("Press [L] to go to the previous stage.");
				helpText.push_back("Press [Down] to reset the camera white balance.");
				helpText.push_back("Press [Up] to calibrate the grid anchoring");
				helpText.push_back("(do this while looking directly down on the markers).");

				helpWindowBackground.set_width(DISPLAY_WIDTH * 0.75f);
				break;
			case GenerationStage::FinishedGeneration:
				helpText.push_back("The level has been generated and is ready to be played!");
				helpText.push_back("");
				helpText.push_back("Press [CROSS] or [R] to start the game.");
				helpText.push_back("Press [L] to go to the previous stage.");
				helpText.push_back("Press [Down] to reset the camera white balance.");
				helpText.push_back("Press [Up] to calibrate the grid anchoring");
				helpText.push_back("(do this while looking directly down on the markers).");

				helpWindowBackground.set_width(DISPLAY_WIDTH * 0.7f);
				break;
			default:
				break;
		}
	}

	// While in gameplay.
	if (levelGenerator.finishedGeneration) {
		switch (gameplayManager.GetStage()) {
			case GameplayStage::Build:
				helpText.push_back("This is the build stage, in which you can build towers");
				helpText.push_back("to protect your home from attacking enemies.");
				helpText.push_back("");
				helpText.push_back("Press [SQUARE] to place a tower, if you have enough money.");
				helpText.push_back("Press [CROSS] to start the next wave.");
				helpText.push_back("Press [Down] to reset the camera white balance.");
				helpText.push_back("Press [Up] to calibrate the grid anchoring");
				helpText.push_back("(do this while looking directly down on the markers).");

				helpWindowBackground.set_width(DISPLAY_WIDTH * 0.75f);
				break;
			case GameplayStage::Defend:
				helpText.push_back("This is the defend stage. Your towers will automatically");
				helpText.push_back("attack nearby enemies. Sit back and watch how it plays out.");
				helpText.push_back("");
				helpText.push_back("Press [Down] to reset the camera white balance.");
				helpText.push_back("Press [Up] to calibrate the grid anchoring");
				helpText.push_back("(do this while looking directly down on the markers).");

				helpWindowBackground.set_width(DISPLAY_WIDTH * 0.75f);
				break;
			case GameplayStage::Result:
				helpText.push_back("This is the result stage. It shows the results of the");
				helpText.push_back("previous wave.");
				helpText.push_back("");
				if (gameplayManager.alive) {
					helpText.push_back("Press [CROSS] to move to the build stage.");
					helpText.push_back("Press [CIRCLE] to end the game.");
				} else {
					helpText.push_back("Press [CROSS] or [CIRCLE] to end the game.");
				}
				helpText.push_back("Press [Down] to reset the camera white balance.");
				helpText.push_back("Press [Up] to calibrate the grid anchoring");
				helpText.push_back("(do this while looking directly down on the markers).");

				helpWindowBackground.set_width(DISPLAY_WIDTH * 0.75f);
				break;
			default:
				break;
		}
	}

	// Set the height and position of the background window.
	helpWindowBackground.set_height((helpText.size() + 1) * 20);
	helpWindowBackground.set_position(gef::Vector4(DISPLAY_WIDTH / 2, 90 + helpWindowBackground.height() / 2, 0));

	// Render the help window.
	spriteRenderer->DrawSprite(helpWindowBackground);

	// Render the help text.
	for (int i = 0; i < helpText.size(); i++) {
		font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH / 2, 100 + (i * 20), 0), 0.5f, 0xffffffff, gef::TextJustification::TJ_CENTRE, helpText[i].c_str());
	}
}