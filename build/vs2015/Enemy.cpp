#include "Enemy.h"

// --CONSTRUCTORS--

ds::Enemy::Enemy() {
}

ds::Enemy::Enemy(gef::Mesh* mesh, gef::Matrix44* anchor, gef::Vector4 position, gef::Vector4* worldScale) : GameObject3D(mesh, anchor, position, worldScale, false) {
}

ds::Enemy::~Enemy() {
}

// -----PUBLIC-----

// Initialises the enemy.
void ds::Enemy::Initialise(std::vector<ds::TileGameObject*> path, float speed, float healthPoints) {
	// Initialise waypoints.
	waypoints = path;
	waypointIndex = 1;

	// Initialise attributes.
	movementSpeed = speed;
	health = healthPoints;

	// Set start position.
	startPosition = waypoints[0]->GetPosition();

	// Initialise variables.
	timePassed = 0;
	destroy = false;
}

// NOTE: Possibly defunct, should be moved into OUpdate method.
// Updates the start position of the enemy.
void ds::Enemy::UpdateStartPosition() {
	startPosition = waypoints[waypointIndex - 1]->GetPosition();
}

// -----PRIVATE----

// Updates the enemy.
void ds::Enemy::OverrideableUpdate(float deltaTime) {
	if (waypointIndex < waypoints.size()) {
		// Get distance between position and next waypoint.
		float distance = ds::utils::GetDistanceBetweenPoints(GetPosition(), waypoints[waypointIndex]->GetPosition());

		if (distance < 0.001f) {
			// Change waypoint.
			SetPosition(waypoints[waypointIndex]->GetPosition());
			startPosition = GetPosition();
			waypointIndex++;
			timePassed = 0;
		} else {
			// Lerp towards waypoint.
			gef::Vector4 newPosition;

			timePassed += deltaTime * 0.5f * movementSpeed;
			newPosition.Lerp(startPosition, waypoints[waypointIndex]->GetPosition(), timePassed);

			SetPosition(newPosition);
		}
	} else {
		destroy = true;
	}
}