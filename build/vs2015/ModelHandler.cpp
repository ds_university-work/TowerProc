#include "ModelHandler.h"
#include <stdexcept>

// --CONSTRUCTORS--

ds::ModelHandler::ModelHandler(gef::Platform* platform_) {
	modelLookup = std::vector<ModelRef>();
	sceneLookup = std::vector<SceneRef>();
	platform = platform_;
}

ds::ModelHandler::~ModelHandler() {
	ReleaseResources();
}

// -----PUBLIC-----

// Loads in a model from an .obj file.
ds::ModelRef ds::ModelHandler::LoadModelObj(std::string filename, std::string modelName) {
	// Create a new element in the model lookup to store the loaded model.
	modelLookup.push_back(ModelRef());

	// Create an object loader.
	gef::OBJLoader objLoader;

	// If file exists, load the model into memory; else throw an error.
	modelLookup.back().model = new gef::Model();

	std::string filepath = "Assets/Models/" + filename;

	if (objLoader.Load(filepath.c_str(), *platform, *modelLookup.back().model)) {
		modelLookup.back().modelName = modelName;

		return modelLookup.back();
	} else {
		modelLookup.pop_back();

		//throw std::runtime_error("File does not exist: " + filename);
	}
	return ds::ModelRef();
}

// Loads in a scene mesh from a .scn file.
ds::SceneRef ds::ModelHandler::LoadModelScn(std::string filename, std::string modelName) {
	// Create a new element in the scene mesh lookup to store the loaded scene mesh.
	sceneLookup.push_back(SceneRef());

	sceneLookup.back().sceneMesh.scene = new gef::Scene();

	std::string filepath = "Assets/Models/" + filename;

	// If file exists, load the scene mesh into memory; else throw an error.
	if (sceneLookup.back().sceneMesh.scene->ReadSceneFromFile(*platform, filepath.c_str())) {
		// Create materials from the material data in the scene file.
		sceneLookup.back().sceneMesh.scene->CreateMaterials(*platform);

		// If the scene contains a mesh, load the scene mesh into memory; else throw an error.
		if (sceneLookup.back().sceneMesh.scene->meshes.size() > 0) {
			sceneLookup.back().sceneMesh.mesh = sceneLookup.back().sceneMesh.scene->CreateMesh(*platform, sceneLookup.back().sceneMesh.scene->meshes.front());
			sceneLookup.back().meshName = modelName;

			return sceneLookup.back();
		} else {
			//throw std::runtime_error("File contains no meshes: " + filename);
		}
	} else {
		//throw std::runtime_error("File does not exist: " + filename);
	}
	return ds::SceneRef();
}

// Returns a mesh fron a given name.
gef::Mesh* ds::ModelHandler::GetMesh(std::string meshName) {
	// Check the model lookup table.
	for (int i = 0; i < modelLookup.size(); i++) {
		if (modelLookup[i].modelName == meshName) {
			return modelLookup[i].model->mesh();
		}
	}

	// Check the scene mesh lookup table.
	for (int i = 0; i < sceneLookup.size(); i++) {
		if (sceneLookup[i].meshName == meshName) {
			return sceneLookup[i].sceneMesh.mesh;
		}
	}

	// Mesh does not exist.
	return NULL;
}

// -----PRIVATE----

// Releases any used resources.
void ds::ModelHandler::ReleaseResources() {
	// Release the stored models.
	for (int i = 0; i < modelLookup.size(); i++) {
		delete modelLookup[i].model;
	}
	modelLookup.clear();

	// Clean up the scene meshes.
	for (int i = 0; i < sceneLookup.size(); i++) {
		delete sceneLookup[i].sceneMesh.mesh;
		sceneLookup[i].sceneMesh.mesh = NULL;

		delete sceneLookup[i].sceneMesh.scene;
		sceneLookup[i].sceneMesh.scene = NULL;
	}
}