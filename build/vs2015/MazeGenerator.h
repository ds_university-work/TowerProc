#ifndef MAZEGENERATOR_H
#define MAZEGENERATOR_H

#include <random>
#include <time.h>
#include <vector>

#include "GenerationTile.h"
#include "MazeGrid.h"

namespace ds {
namespace procgen {

// Enum for defining a cardinal direction.
// ================
enum Direction {
	North = 0,
	South = 1,
	East  = 2,
	West  = 3
};

// Struct for defining a direction and its validity.
// ================
struct ValidDirection {
	ValidDirection(Direction d) { direction = d; valid = true; }
	ValidDirection() { valid = false; }

	Direction direction;
	bool valid;
};

// Class for generating a 2D, grid-based maze.
// ================
class MazeGenerator {
public:
	MazeGenerator();
	~MazeGenerator();

	MazeGrid GenerateMaze(gef::Vector2 gridSize, gef::Vector2 home, std::vector<gef::Vector2> enemySpawns);

private:
	void WalkPhase();
	void HuntPhase();
	void CalculateEnemyPaths(std::vector<gef::Vector2> enemySpawns);

	ValidDirection GetValidRandomDirection(ds::procgen::GenerationTileType type);
	bool IsDirectionValid(Direction d, ds::procgen::GenerationTileType type);
	int GetDirectionValueX(Direction d);
	int GetDirectionValueY(Direction d);

	MazeGrid grid;
	bool endReached;

	gef::Vector2 current;
};

}
}

#endif