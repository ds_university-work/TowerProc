#ifndef TOWER_H
#define TOWER_H

#include "GameObject3D.h"

#include "AudioHandler.h"
#include "Enemy.h"
#include "Collision.h"
#include "ModelHandler.h"
#include "Projectile.h"
#include "TileGameObject.h"

namespace ds {

// Forward declarations.
class TileGameObject;
class Enemy;
class Projectile;

// NOTE: Essentially useless right now.
// Enum to represent tower type.
// ================
enum TowerType {
	Gun,
	Bomb
};

// Game object class to represent a fixed player tower/turret.
class Tower : public GameObject3D {
public:
	Tower();
	Tower(gef::Mesh* mesh, gef::Mesh* topMesh, gef::Matrix44* anchor, gef::Vector4 position, gef::Vector4* worldScale);
	~Tower();

	void Initialise(TowerType towerType, std::vector<Projectile>* projectiles, ModelHandler* modelHandler_, AudioHandler* audioHandler_);
	void SetTarget(Enemy* enemy);
	void UpdateTarget(std::vector<ds::Enemy>* enemies);

	GameObject3D top;

	int shotsFired;

private:
	void OverrideableUpdate(float deltaTime);
	void Fire();
	void ResetFireDelay();

	ModelHandler* modelHandler;
	AudioHandler* audioHandler;

	TowerType type;
	std::vector<Projectile>* projectileArray;
	ds::Enemy* target;
	float range;
	float fireDelay;
	float damage;

};

}

#endif