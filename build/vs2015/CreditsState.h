#ifndef CREDITSSTATE_H
#define CREDITSSTATE_H

#include "BaseState.h"

namespace ds {
namespace state {

enum GameState;

// Credits screen game state.
// ================
class CreditsState : public ds::state::BaseState {
public:
	CreditsState();
	~CreditsState();

	void Initialise();
	void Update(float deltaTime, AppData* dat);
	void Render(AppData* dat);
	void CleanUp();

	GameState GetNextState();

private:
	gef::Sprite background;
	gef::Sprite devLogo;
	gef::Sprite gameLogo;

	std::string creditsTitleText;
	std::vector<std::string> creditsText;

};

}
}

#endif