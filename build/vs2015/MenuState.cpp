#include "MenuState.h"

// --CONSTRUCTORS--

ds::state::MenuState::MenuState() {
}

ds::state::MenuState::~MenuState() {
}

// -----PUBLIC-----

// Initialises the menu state.
void ds::state::MenuState::Initialise() {
	// Set up sprites.
	textureHandler->SetUpSprite(&background,
		"Background",
		gef::Vector4(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2, 0),
		gef::Vector2(DISPLAY_WIDTH, DISPLAY_HEIGHT));

	textureHandler->SetUpSprite(&gameLogo,
		"GameLogo",
		gef::Vector4(DISPLAY_WIDTH / 4, DISPLAY_HEIGHT / 2, 0),
		gef::Vector2(DISPLAY_WIDTH / 2.5f, DISPLAY_WIDTH / 2.5f));

	button.set_texture(textureHandler->GetTexture("MenuButton"));
	button.set_width(DISPLAY_WIDTH / 2.5f);
	button.set_height(DISPLAY_HEIGHT / 8);

	textureHandler->SetUpSprite(&audioTick,
		"MenuOptionUnticked",
		gef::Vector4(DISPLAY_WIDTH / 8 * 7, DISPLAY_HEIGHT / 5 * 2, 0),
		gef::Vector2(DISPLAY_HEIGHT / 8, DISPLAY_HEIGHT / 8));

	highlight.set_texture(textureHandler->GetTexture("MenuHighlight"));
	highlight.set_width(32);
	highlight.set_height(32);

	// Set text.
	startText = "START";
	audioMuteText = "Mute audio?";
	howToPlayText = "HOW TO PLAY";
	creditsText = "CREDITS";

	// Initialise variables.
	selectedOptionIndex = 0;
	ChangeOption();

	// Initialise variables.
	showHowToPlay = false;
	audioMuted = false;

	// Konami code... code.
	ResetKonami();
}

// Updates the menu state.
void ds::state::MenuState::Update(float deltaTime, AppData* dat) {
	// If main menu is shown.
	if (!showHowToPlay) {
		// Perform an action.
		if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_CROSS) {
			DoAction();
			audioHandler->PlaySample("MenuBeep");
		}

		// Move the selected option.
		if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_UP) {
			selectedOptionIndex--;
			ChangeOption();
			audioHandler->PlaySample("MenuBeep");
		}
		if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_DOWN) {
			selectedOptionIndex++;
			ChangeOption();
			audioHandler->PlaySample("MenuBeep");
		}
	} else {
		// Hide the how to play screen.
		if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_CIRCLE) {
			showHowToPlay = false;
			audioHandler->PlaySample("MenuBeep");
		}
	}

	// Update the konami code.
	UpdateKonami();
}

// Renders the menu state.
void ds::state::MenuState::Render(AppData* dat) {
	// Start rendering 2D.
	spriteRenderer->Begin(true);

	// Render background.
	spriteRenderer->DrawSprite(background);

	// Render logo.
	spriteRenderer->DrawSprite(gameLogo);

	if (!showHowToPlay) {
		// Start button.
		gef::Vector4 startButtonPosition = gef::Vector4(DISPLAY_WIDTH / 4 * 3, DISPLAY_HEIGHT / 5 * 1, 0);
		button.set_position(startButtonPosition);
		spriteRenderer->DrawSprite(button);

		// Start button text.
		startButtonPosition = gef::Vector4(startButtonPosition.x(), startButtonPosition.y() - 20, startButtonPosition.z());
		font->RenderText(spriteRenderer, startButtonPosition, 0.9f, 0xffffffff, gef::TextJustification::TJ_CENTRE, startText.c_str());

		// Mute audio tick.
		spriteRenderer->DrawSprite(audioTick);

		// Mute audio text.
		gef::Vector4 audioTextPosition = gef::Vector4(DISPLAY_WIDTH / 4 * 2.25f, DISPLAY_HEIGHT / 5 * 2 - 20, 0);
		font->RenderText(spriteRenderer, audioTextPosition, 0.9f, 0xffffffff, gef::TextJustification::TJ_LEFT, audioMuteText.c_str());

		// How to play button.
		gef::Vector4 howToPlayButtonPosition = gef::Vector4(DISPLAY_WIDTH / 4 * 3, DISPLAY_HEIGHT / 5 * 3, 0);
		button.set_position(howToPlayButtonPosition);
		spriteRenderer->DrawSprite(button);

		// How to play button text.
		howToPlayButtonPosition = gef::Vector4(howToPlayButtonPosition.x(), howToPlayButtonPosition.y() - 20, howToPlayButtonPosition.z());
		font->RenderText(spriteRenderer, howToPlayButtonPosition, 0.9f, 0xffffffff, gef::TextJustification::TJ_CENTRE, howToPlayText.c_str());

		// Credits button.
		gef::Vector4 creditsButtonPosition = gef::Vector4(DISPLAY_WIDTH / 4 * 3, DISPLAY_HEIGHT / 5 * 4, 0);
		button.set_position(creditsButtonPosition);
		spriteRenderer->DrawSprite(button);

		// Credits button text.
		creditsButtonPosition = gef::Vector4(creditsButtonPosition.x(), creditsButtonPosition.y() - 20, creditsButtonPosition.z());
		font->RenderText(spriteRenderer, creditsButtonPosition, 0.9f, 0xffffffff, gef::TextJustification::TJ_CENTRE, creditsText.c_str());

		// Selection highlight.
		highlight.set_position(gef::Vector4(DISPLAY_WIDTH / 2 , DISPLAY_HEIGHT / 5 * (selectedOptionIndex + 1), 0));
		spriteRenderer->DrawSprite(highlight);
	} else {
		// How to play title text.
		font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH / 4 * 3 - 20, DISPLAY_HEIGHT / 5 * 1 - 80, 0), 2.0f, 0xffffffff, gef::TextJustification::TJ_CENTRE, howToPlayText.c_str());

		// How to play details text.
		howToPlayDetailsText.clear();

		howToPlayDetailsText.push_back("The game is played similar to any other tower");
		howToPlayDetailsText.push_back("defense game. Enemies will spawn and will");
		howToPlayDetailsText.push_back("follow a path towards your home. Place towers");
		howToPlayDetailsText.push_back("using gained money to stop them in their tracks.");
		howToPlayDetailsText.push_back("");
		howToPlayDetailsText.push_back("The PS Vita markers 1-4 are needed for this.");
		howToPlayDetailsText.push_back("Markers #1 & #2 will be used to determine the");
		howToPlayDetailsText.push_back("size of the level (#1 as the bottom left, #2 as");
		howToPlayDetailsText.push_back("the top right), and as reference points for the");
		howToPlayDetailsText.push_back("camera tracking. Marker #3 is used to place the");
		howToPlayDetailsText.push_back("home point, and marker #4 is used to place enemy");
		howToPlayDetailsText.push_back("spawners (currently only one spawner should be");
		howToPlayDetailsText.push_back("placed). It is recommended to use something to");
		howToPlayDetailsText.push_back("hold markers #1 and #2 in place, such as blu-tac.");
		howToPlayDetailsText.push_back("");
		howToPlayDetailsText.push_back("Help can be given at any point of the game by");
		howToPlayDetailsText.push_back("holding down the [TRIANGLE] button.");
		howToPlayDetailsText.push_back("");
		howToPlayDetailsText.push_back("(Easter egg: try performing the Konami code while");
		howToPlayDetailsText.push_back("on the menu screen).");

		// Render how to play details.
		for (int i = 0; i < howToPlayDetailsText.size(); i++) {
			font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 5 * 2 - 100 + i * 20, 0), 0.45f, 0xffffffff, gef::TextJustification::TJ_LEFT, howToPlayDetailsText[i].c_str());
		}

	}

	// Stop rendering 2D
	spriteRenderer->End();
}

// Release any used resources.
void ds::state::MenuState::CleanUp() {
}

// Returns the state to change to.
ds::state::GameState ds::state::MenuState::GetNextState() {
	return selectedOption;
}

// -----PRIVATE----

// Changes the currently selected option.
void ds::state::MenuState::ChangeOption() {
	// Ensure the index stays valid.
	if (selectedOptionIndex < 0) {
		selectedOptionIndex = 0;
	} else if (selectedOptionIndex >= 4) {
		selectedOptionIndex = 3;
	}

	// Change option to the next state.
	switch (selectedOptionIndex) {
		case 0:
			selectedOption = ds::state::GameState::GS_InGameState;
			break;
		case 1:
			// Do nothing
			break;
		case 2:
			// Do nothing
			break;
		case 3:
			selectedOption = ds::state::GameState::GS_CreditsState;
			break;
		default:
			selectedOption = ds::state::GameState::GS_InGameState;
			break;
	}
}

// Performs the currently selection action.
void ds::state::MenuState::DoAction() {
	switch (selectedOptionIndex) {
		case 0:
			changeState = true;
			break;
		case 1:
			audioMuted = !audioMuted;

			if (audioMuted) {
				audioTick.set_texture(textureHandler->GetTexture("MenuOptionTicked"));
			} else {
				audioTick.set_texture(textureHandler->GetTexture("MenuOptionUnticked"));
			}

			audioHandler->muted = audioMuted;
			break;
		case 2:
			showHowToPlay = true;
			break;
		case 3:
			changeState = true;
			break;
	}
}

// Updates the Konami code... code.
void ds::state::MenuState::UpdateKonami() {
	// If a keypress has been made.
	if (inputManager->controller_input()->GetController(0)->buttons_pressed() != 0) {
		// Check if the correct key has been pressed.
		switch (konamiIndex) {
			case 0:
				CheckKonamiKey(konamiIndex, gef_SONY_CTRL_UP);				// UP
				break;
			case 1:
				CheckKonamiKey(konamiIndex, gef_SONY_CTRL_UP);				// UP
				break;
			case 2:
				CheckKonamiKey(konamiIndex, gef_SONY_CTRL_DOWN);			// DOWN
				break;
			case 3:
				CheckKonamiKey(konamiIndex, gef_SONY_CTRL_DOWN);			// DOWN
				break;
			case 4:
				CheckKonamiKey(konamiIndex, gef_SONY_CTRL_LEFT);			// LEFT
				break;
			case 5:
				CheckKonamiKey(konamiIndex, gef_SONY_CTRL_RIGHT);			// RIGHT
				break;
			case 6:
				CheckKonamiKey(konamiIndex, gef_SONY_CTRL_LEFT);			// LEFT
				break;
			case 7:
				CheckKonamiKey(konamiIndex, gef_SONY_CTRL_RIGHT);			// RIGHT
				break;
			case 8:
				CheckKonamiKey(konamiIndex, gef_SONY_CTRL_CIRCLE);			// CIRCLE
				break;
			case 9:
				CheckKonamiKey(konamiIndex, gef_SONY_CTRL_CROSS);			// CROSS
				break;
			case 10:
				CheckKonamiKey(konamiIndex, gef_SONY_CTRL_START);			// START
				break;
		}
	}

	// If the konami code has not been activated.
	if (!konamiActive) {
		// Check if the Konami code has been entered.
		bool result = true;
		for (int i = 0; i < 11; i++) {
			if (!konami[i]) {
				result = false;
			}
		}

		// If the Konami code has been entered, perform an action (play a sound).
		if (result) {
			konamiActive = true;

			audioHandler->PlaySample("Airhorn");
		}
	}
}

// Checks if the correct key has been pressed.
void ds::state::MenuState::CheckKonamiKey(int index, int key) {
	// Check if the next key in the squence has been pressed.
	if (!konami[index]) {
		if (inputManager->controller_input()->GetController(0)->buttons_pressed() == key) {
			konami[index] = true;

			konamiIndex++;
		} else {
			ResetKonami();
		}
	}


}

// Resets the Konami code.
void ds::state::MenuState::ResetKonami() {
	konamiActive = false;

	for (int i = 0; i < 11; i++) {
		konami[i] = false;
	}

	konamiIndex = 0;
}