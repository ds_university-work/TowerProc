#ifndef TEXTUREHANDLER_H
#define TEXTUREHANDLER_H

#include <string>
#include "graphics/sprite.h"
#include "graphics/texture.h"
#include "graphics/image_data.h"
#include "assets/png_loader.h"
#include "maths/vector2.h"
#include "maths/vector4.h"
#include <vector>

namespace ds {

struct TextureRef {
	std::string textureName;
	gef::Texture* texture;
};

class TextureHandler {
public:
	TextureHandler(gef::Platform* platform_);
	~TextureHandler();

	TextureRef LoadTexturePNG(std::string filename, std::string textureName);
	gef::Texture* GetTexture(std::string textureName);
	void SetUpSprite(gef::Sprite* sprite, std::string textureName, gef::Vector4 position, gef::Vector2 dimensions);

	void ReleaseResources();

private:
	gef::Platform* platform;
	gef::PNGLoader pngLoader;

	std::vector<TextureRef> textureLookup;
};

}

#endif