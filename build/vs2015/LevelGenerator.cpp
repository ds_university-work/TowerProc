#include "LevelGenerator.h"

// --CONSTRUCTORS--

ds::LevelGenerator::LevelGenerator() {
}

ds::LevelGenerator::~LevelGenerator() {
}

// -----PUBLIC-----

// Initialises the level generator.
void ds::LevelGenerator::Initialise(ModelHandler* modelHandler_, ARCamera* arCamera_, Level* level, gef::Vector4* worldScale_) {
	// Link handlers.
	modelHandler = modelHandler_;
	arCamera = arCamera_;
	gameLevel = level;
	worldScale = worldScale_;

	// Set generation stage.
	finishedGeneration = false;
	generationStage = GenerationStage::SettingGridSize;

	// Initialise variables.
	homePlaced = false;
	enemySpawnPlaced = false;
	pathGenerated = false;
	tileSeparation = 0.025f;

	// Initialise generation objects.
	homeIndicator = GameObject3D(modelHandler->GetMesh("Cube"), &arCamera->GetMarker(AR_MARKER_3)->transform, gef::Vector4(0, 0, 0.03f), worldScale);
	homeIndicator.SetScale(gef::Vector4(0.2f, 0.2f, 4.0f));

	enemySpawnIndicator = GameObject3D(modelHandler->GetMesh("Cube"), &arCamera->GetMarker(AR_MARKER_4)->transform, gef::Vector4(0, 0, 0.03f), worldScale);
	enemySpawnIndicator.SetScale(gef::Vector4(0.2f, 0.2f, 4.0f));

}

// Updates the current generation stage.
void ds::LevelGenerator::UpdateStage(float deltaTime) {
	// If setting the grid size, update the grid.
	if (generationStage == GenerationStage::SettingGridSize) {
		if (arCamera->GetMarker(AR_MARKER_1)->visible && arCamera->GetMarker(AR_MARKER_2)->visible) {
			// Calibrate the grid using the position of the markers.
			arCamera->CalibrateGridMarkers();

			// Update the grid size
			UpdateGridSize();
		}
	}

	// Update game level objects.
	gameLevel->UpdateObjects(deltaTime);

	// Update generation indicators.
	homeIndicator.Update(deltaTime);
	enemySpawnIndicator.Update(deltaTime);
}

// Performs the action of the generation stage.
void ds::LevelGenerator::DoStageAction() {
	switch (generationStage) {
		case GenerationStage::SettingGridSize:
			// Change stage.
			NextStage();
			break;
		case GenerationStage::PlacingHomeTile:
			// Set player home tile.
			PlaceHomeTile();
			break;
		case GenerationStage::PlacingEnemySpawners:
			// Add or remove an enemy spawner.
			AddRemoveEnemySpawner();
			break;
		case GenerationStage::ChoosingGeneratedPath:
			// Generate a new random path.
			GeneratePath();
			break;
		case GenerationStage::FinishedGeneration:
			// Finish level generation.
			finishedGeneration = true;
			break;
	}
}

// Moves to the next stage.
void ds::LevelGenerator::NextStage() {
	switch (generationStage) {
		case GenerationStage::SettingGridSize:
			gameLevel->UpdateGridAnchoring(true);
			generationStage = GenerationStage::PlacingHomeTile;
			break;
		case GenerationStage::PlacingHomeTile:
			if (homePlaced) {
				generationStage = GenerationStage::PlacingEnemySpawners;
			}
			break;
		case GenerationStage::PlacingEnemySpawners:
			if (enemySpawnPlaced) {
				generationStage = GenerationStage::ChoosingGeneratedPath;
			}
			break;
		case GenerationStage::ChoosingGeneratedPath:
			if (pathGenerated) {
				generationStage = GenerationStage::FinishedGeneration;
			}
			break;
		case GenerationStage::FinishedGeneration:
			DoStageAction();
			break;
	}
}

// Moves to the previous stage.
void ds::LevelGenerator::PreviousStage() {
	switch (generationStage) {
		case GenerationStage::SettingGridSize:
			// Do nothing.
			break;
		case GenerationStage::PlacingHomeTile:
			generationStage = GenerationStage::SettingGridSize;
			break;
		case GenerationStage::PlacingEnemySpawners:
			generationStage = GenerationStage::PlacingHomeTile;
			break;
		case GenerationStage::ChoosingGeneratedPath:
			generationStage = GenerationStage::PlacingEnemySpawners;
			break;
		case GenerationStage::FinishedGeneration:
			generationStage = GenerationStage::ChoosingGeneratedPath;
			break;
	}
}

// Returns the current generation stage.
ds::GenerationStage ds::LevelGenerator::GetStage() {
	return generationStage;
}

// Renders the 3D aspects of the level generator.
void ds::LevelGenerator::Render3D(gef::Renderer3D* renderer3D) {
	// Render grid if both markers can be seen and the grid size is being set.
	if (generationStage == GenerationStage::SettingGridSize &&
		arCamera->GetMarker(AR_MARKER_1)->visible &&
		arCamera->GetMarker(AR_MARKER_2)->visible) {
		// Render grid tiles.
		for (int y = 0; y < gameLevel->grid.size(); y++) {
			for (int x = 0; x < gameLevel->grid[y].size(); x++) {
				renderer3D->DrawMesh(gameLevel->grid[y][x]);
			}
		}
	}

	// Render game level if grid size has been set and a marker can be seen.
	if (generationStage != GenerationStage::SettingGridSize) {
		if (arCamera->GetMarker(AR_MARKER_1)->visible || arCamera->GetMarker(AR_MARKER_2)->visible) {
			// Render grid.
			for (int y = 0; y < gameLevel->grid.size(); y++) {
				for (int x = 0; x < gameLevel->grid[y].size(); x++) {
					renderer3D->DrawMesh(gameLevel->grid[y][x]);
				}
			}

			// Render home object.
			if (homePlaced) {
				renderer3D->DrawMesh(gameLevel->homeObject);
			}

			// Render spawner objects.
			if (enemySpawnPlaced) {
				for (int i = 0; i < gameLevel->enemySpawnerObjects.size(); i++) {
					renderer3D->DrawMesh(gameLevel->enemySpawnerObjects[i]);
				}
			}

			// Render level generation home indicator.
			if (generationStage == GenerationStage::PlacingHomeTile &&
				arCamera->GetMarker(AR_MARKER_3)->visible) {
				renderer3D->DrawMesh(homeIndicator);
			}

			// Render level generation enemy spawn indicator.
			if (generationStage == GenerationStage::PlacingEnemySpawners &&
				arCamera->GetMarker(AR_MARKER_4)->visible) {
				renderer3D->DrawMesh(enemySpawnIndicator);
			}
		}
	}
}

// Renders the 2D aspects of the level generator.
void ds::LevelGenerator::Render2D(gef::SpriteRenderer* spriteRenderer, gef::Font* font) {
	// Get generation stage text.
	std::string stageText = "";
	std::string previousStageText = "<- ";
	std::string nextStageText = "";

	switch (generationStage) {
		case GenerationStage::SettingGridSize:
			previousStageText = "";
			stageText += "Set Grid Size";
			nextStageText += "Place Home Tile";
			break;
		case GenerationStage::PlacingHomeTile:
			previousStageText += "Set Grid Size";
			stageText += "Place Home Tile";
			nextStageText += "Place Enemy Spawners";
			break;
		case GenerationStage::PlacingEnemySpawners:
			previousStageText += "Place Home Tile";
			stageText += "Place Enemy Spawners";
			nextStageText += "Generate Path";
			break;
		case GenerationStage::ChoosingGeneratedPath:
			previousStageText += "Place Enemy Spawners";
			stageText += "Generate Path";
			nextStageText += "Finish";
			break;
		case GenerationStage::FinishedGeneration:
			previousStageText += "Generate Path";
			stageText += "Finished Generation";
			nextStageText += "Play";
			break;
		default:
			stageText += "stageText overflow";
			break;
	}
	nextStageText += " ->";

	// Render generation stage.
	font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH / 2, 0, 0), 0.75f, 0xffffffff, gef::TextJustification::TJ_CENTRE, "Level Generation");
	font->RenderText(spriteRenderer, gef::Vector4(0, 30, 0), 0.5f, 0xffffffff, gef::TextJustification::TJ_LEFT, previousStageText.c_str());
	font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH / 2, 30, 0), 0.5f, 0xffffffff, gef::TextJustification::TJ_CENTRE, stageText.c_str());
	font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH, 30, 0), 0.5f, 0xffffffff, gef::TextJustification::TJ_RIGHT, nextStageText.c_str());
}

// -----PRIVATE----

// Updates the size of the level grid.
void ds::LevelGenerator::UpdateGridSize() {
	// Create temporary grid.
	std::vector<std::vector<ds::TileGameObject>> grid;

	// Calculate the grid size from the marker distances.
	gameLevel->gridSize.x = std::abs(std::round(arCamera->GetGridMarkerDistance().x / (tileSeparation * 2)) + 1);
	gameLevel->gridSize.y = std::abs(std::round(arCamera->GetGridMarkerDistance().y / (tileSeparation * 2)) + 1);

	// Double the grid size (almost).
	int xSize = gameLevel->gridSize.x * 2 - 1;
	int ySize = gameLevel->gridSize.y * 2 - 1;

	// Clear the object array.
	grid.clear();

	// Create and position the grid tiles.
	for (int y = 0; y < ySize; y++) {
		grid.push_back(std::vector<ds::TileGameObject>());
		for (int x = 0; x < xSize; x++) {
			// Calculate the object position.
			gef::Vector4 position = gef::Vector4(x * tileSeparation, y * tileSeparation, -0.001f);

			// Instantiate tile.
			grid[y].push_back(ds::TileGameObject(modelHandler->GetMesh("TileGround"), &arCamera->GetMarker(0)->transform, position, worldScale));
			grid[y].back().SetScale(gef::Vector4(1, 1, 1));
			grid[y].back().Initialise(gef::Vector2(x, y), ds::TileType::Ground);
		}
	}

	// Set the grid.
	gameLevel->SetGrid(grid);
}

// Places the player home tile.
void ds::LevelGenerator::PlaceHomeTile() {
	if (arCamera->GetMarker(AR_MARKER_3)->visible) {
		// Check every tile to see if it is colliding with the home indicator.
		for (int y = 0; y < gameLevel->grid.size(); y++) {
			for (int x = 0; x < gameLevel->grid[y].size(); x++) {
				if (ds::utils::CheckCollisionAABB(&homeIndicator, &gameLevel->grid[y][x])) {
					gameLevel->SetHome(gef::Vector2(x, y));
					homePlaced = true;

					return;
				}
			}
		}
	}
}

// Adds or removes an enemy spawner.
void ds::LevelGenerator::AddRemoveEnemySpawner() {
	if (arCamera->GetMarker(AR_MARKER_4)->visible) {
		// Check every tile to see if it is colliding with the enemy spawner indicator
		for (int y = 0; y < gameLevel->grid.size(); y++) {
			for (int x = 0; x < gameLevel->grid[y].size(); x++) {
				if (ds::utils::CheckCollisionAABB(&enemySpawnIndicator, &gameLevel->grid[y][x])) {
					bool addedSpawner = gameLevel->AddRemoveEnemySpawn(gef::Vector2(x, y));

					// Check if there are any enemy spawns placed.
					if (gameLevel->enemySpawnerCoordinates.size() <= 0) {
						enemySpawnPlaced = false;
					} else {
						enemySpawnPlaced = true;
					}

					return;
				}
			}
		}
	}
}

// Generate a new path from the enemy spawners to the player home.
void ds::LevelGenerator::GeneratePath() {
	// Generate the maze and path.
	if (gameLevel->gridSize.x > 1 && gameLevel->gridSize.y > 1) {
		// Generate the level maze.
		mazeGrid = mazeGenerator.GenerateMaze(gameLevel->gridSize, gameLevel->homeCoordinate, gameLevel->enemySpawnerCoordinates);

		// Calculate the enemy paths.
		gameLevel->enemyPath = mazeGrid.GetEnemyPath();
	}

	// Initialise all tiles to Ground.
	for (int y = 0; y < gameLevel->grid.size(); y++) {
		for (int x = 0; x < gameLevel->grid[y].size(); x++) {
			gameLevel->grid[y][x].SetTileType(ds::TileType::Ground);
			gameLevel->grid[y][x].set_mesh(modelHandler->GetMesh("TileGround"));
		}
	}

	// Set path tile.
	for (int i = 0; i < gameLevel->enemyPath.size(); i++) {
		gameLevel->grid[gameLevel->enemyPath[i].y][gameLevel->enemyPath[i].x].SetTileType(ds::TileType::EnemyPath);
		gameLevel->grid[gameLevel->enemyPath[i].y][gameLevel->enemyPath[i].x].set_mesh(modelHandler->GetMesh("TilePath"));
	}

	// Set home tile.
	gameLevel->grid[gameLevel->homeCoordinate.y][gameLevel->homeCoordinate.x].SetTileType(ds::TileType::Home);

	// Set enemy spawns.
	for (int i = 0; i < gameLevel->enemySpawnerCoordinates.size(); i++) {
		gameLevel->grid[gameLevel->enemySpawnerCoordinates[i].y][gameLevel->enemySpawnerCoordinates[i].x].SetTileType(ds::TileType::Spawn);
	}

	pathGenerated = true;
}