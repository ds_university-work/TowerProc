#include "Collision.h"

// Checks whether two 3D axis-aligned bounding boxes are colliding.
bool ds::utils::CheckCollisionAABB(gef::MeshInstance* meshA, gef::MeshInstance* meshB) {
	// Get world space mesh bounding box
	gef::Aabb wBoundsA = meshA->mesh()->aabb().Transform(meshA->transform());
	gef::Aabb wBoundsB = meshB->mesh()->aabb().Transform(meshB->transform());

	// Check if there is a collision
	if (wBoundsA.max_vtx().x() < wBoundsB.min_vtx().x())
		return false;
	if (wBoundsA.min_vtx().x() > wBoundsB.max_vtx().x())
		return false;
	if (wBoundsA.max_vtx().y() < wBoundsB.min_vtx().y())
		return false;
	if (wBoundsA.min_vtx().y() > wBoundsB.max_vtx().y())
		return false;
	if (wBoundsA.max_vtx().z() < wBoundsB.min_vtx().z())
		return false;
	if (wBoundsA.min_vtx().z() > wBoundsB.max_vtx().z())
		return false;

	return true;
}

// FIXME: Doesn't currently work.
// Checks whether two spheres are colliding.
bool ds::utils::CheckCollisionSphere(GameObject3D goA, GameObject3D goB, bool innerBounds) {
	/*gef::Mesh meshA = goA.GetMesh();
	gef::Matrix44 transformA = goA.LocalTransform;

	gef::Mesh meshB = goB.GetMesh();
	gef::Matrix44 transformB = goB.LocalTransform;

	// Get world space mesh bounding spheres
	gef::Sphere wBoundsA = meshA.bounding_sphere().Transform(transformA);
	gef::Sphere wBoundsB = meshB.bounding_sphere().Transform(transformB);

	if (innerBounds) {
		// Set inner sphere bounds
		// TODO: Change so that it uses the maximum of the minimums of x, y and z.
		float radiusA = (meshA.aabb().max_vtx().x() - meshA.aabb().min_vtx().x()) / 2;
		float radiusB = (meshB.aabb().max_vtx().x() - meshB.aabb().min_vtx().x()) / 2;

		wBoundsA = gef::Sphere(wBoundsA.position(), radiusA);
		wBoundsB = gef::Sphere(wBoundsB.position(), radiusB);
	}

	// Get mesh positions
	const float aX = wBoundsA.position().x();
	const float aY = wBoundsA.position().y();
	const float aZ = wBoundsA.position().z();
	const float bX = wBoundsB.position().x();
	const float bY = wBoundsB.position().y();
	const float bZ = wBoundsB.position().z();

	// Get distance between meshes
	float distance = std::sqrt(std::pow(aX - bX, 2) + std::pow(aY - bY, 2) + std::pow(aZ - bZ, 2));

	// Check if there is a collision
	if (distance > wBoundsA.radius() + wBoundsB.radius())
		return false;

	return true;*/

	return false;
}

// Calculates the distance between two points in 3D space.
float ds::utils::GetDistanceBetweenPoints(gef::Vector4 a, gef::Vector4 b) {
	return sqrt(pow(b.x() - a.x(), 2) + pow(b.y() - a.y(), 2) + pow(b.z() - a.z(), 2));
}