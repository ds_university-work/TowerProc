#include "Camera.h"

// --CONSTRUCTORS--

ds::Camera::Camera() {
}

ds::Camera::~Camera() {
}

// -----PUBLIC-----

// Initialises the camera.
void ds::Camera::SetUpCamera(gef::Vector4 position, gef::Vector4 lookAt, gef::Vector4 up, float fov, float near, float far) {
	SetPosition(position);
	SetLookAt(lookAt);
	SetUpVector(up);
	SetFOV(fov);
	SetFarPlane(far);
	SetNearPlane(near);
}

// Sets the absolute position of the camera.
void ds::Camera::SetPosition(gef::Vector4 position) {
	positionVector = position;
}

// Moves the camera position by a position delta.
void ds::Camera::AdjustPosition(gef::Vector4 positionDelta) {
	SetPosition(positionVector + positionDelta);
}

// Moves the camera position and camera look at by a position delta.
void ds::Camera::PanCamera(gef::Vector4 delta) {
	AdjustPosition(delta);
	AdjustLookAt(delta);
}

// Sets the absolute look at of the camera.
void ds::Camera::SetLookAt(gef::Vector4 lookAt) {
	lookAtVector = lookAt;
}

// Moves the camera look at by a position delta.
void ds::Camera::AdjustLookAt(gef::Vector4 lookAtDelta) {
	SetLookAt(lookAtVector + lookAtDelta);
}

// Sets the camera up vector.
void ds::Camera::SetUpVector(gef::Vector4 up) {
	upVector = up;
}

// Sets the camera field of view.
void ds::Camera::SetFOV(float fov) {
	fieldOfView = fov;
}

// Sets the near view plane of the camera.
void ds::Camera::SetNearPlane(float near) {
	nearPlane = near;
}

// Sets the far view plane of the camera.
void ds::Camera::SetFarPlane(float far) {
	farPlane = far;
}

// Returns the camera position.
gef::Vector4 ds::Camera::GetPosition() {
	return positionVector;
}

// Returns the camera look at.
gef::Vector4 ds::Camera::GetLookAt() {
	return lookAtVector;
}

// Returns the camera up vector.
gef::Vector4 ds::Camera::GetUpVector() {
	return upVector;
}

// Returns the camera field of view.
float ds::Camera::GetFOV() {
	return fieldOfView;
}

// Returns the camera near plane.
float ds::Camera::GetNearPlane() {
	return nearPlane;
}

// Returns the camera far plane.
float ds::Camera::GetFarPlane() {
	return farPlane;
}

// Returns the projection matrix of the camera.
gef::Matrix44 ds::Camera::GetProjectionMatrix(gef::Platform* platform) {
	gef::Matrix44 projectionMatrix = platform->PerspectiveProjectionFov(
		fieldOfView,
		(float)platform->width() / (float)platform->height(),
		nearPlane,
		farPlane);
	return projectionMatrix;
}

// Returns the view matrix of the camera.
gef::Matrix44 ds::Camera::GetViewMatrix() {
	gef::Matrix44 viewMatrix;

	viewMatrix.LookAt(positionVector, lookAtVector, upVector);

	return viewMatrix;
}