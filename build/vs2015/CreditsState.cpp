#include "CreditsState.h"

// --CONSTRUCTORS--

ds::state::CreditsState::CreditsState() {
}

ds::state::CreditsState::~CreditsState() {
}

// -----PUBLIC-----

// Initialises the credits state.
void ds::state::CreditsState::Initialise() {
	// Set up sprites.
	textureHandler->SetUpSprite(&background,
		"Background",
		gef::Vector4(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2, 0),
		gef::Vector2(DISPLAY_WIDTH, DISPLAY_HEIGHT));

	textureHandler->SetUpSprite(&devLogo,
		"DeveloperLogo",
		gef::Vector4(DISPLAY_WIDTH / 4, DISPLAY_HEIGHT / 4, 0),
		gef::Vector2(512, 512));

	textureHandler->SetUpSprite(&gameLogo,
		"GameLogo",
		gef::Vector4(DISPLAY_WIDTH / 4*3, DISPLAY_HEIGHT / 4, 0),
		gef::Vector2(256, 256));

	// Set up credits text.
	creditsTitleText = "CREDITS";

	creditsText.clear();
	creditsText.push_back("\"Tower Proc\", 2016");
	creditsText.push_back("");
	creditsText.push_back("Developed by Dean Sinclair");
	creditsText.push_back("");
	creditsText.push_back("All assets by Dean Sinclair");
	creditsText.push_back("");
	creditsText.push_back("Games Education Framework by Grant Clarke");
	creditsText.push_back("");
	creditsText.push_back("Abertay University");
}

// Updates the credits state.
void ds::state::CreditsState::Update(float deltaTime, AppData* dat) {
	// Go back to the main menu.
	if (inputManager->controller_input()->GetController(0)->buttons_pressed() == gef_SONY_CTRL_CIRCLE) {
		changeState = true;
		audioHandler->PlaySample("MenuBeep");
	}
}

// Renders the credits state.
void ds::state::CreditsState::Render(AppData* dat) {
	// Start rendering 2D.
	spriteRenderer->Begin();

	// Render the background.
	spriteRenderer->DrawSprite(background);

	// Render the developer logo.
	spriteRenderer->DrawSprite(devLogo);

	// Render the game logo.
	spriteRenderer->DrawSprite(gameLogo);

	// Render the title text.
	font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH / 2, 200, 0), 2.0f, 0xffffffff, gef::TextJustification::TJ_CENTRE, creditsTitleText.c_str());

	// Render the credits text.
	for (int i = 0; i < creditsText.size(); i++) {
		font->RenderText(spriteRenderer, gef::Vector4(DISPLAY_WIDTH / 2, 300 + i * 20, 0), 0.75f, 0xffffffff, gef::TextJustification::TJ_CENTRE, creditsText[i].c_str());
	}
	
	// Finish rendering 2D.
	spriteRenderer->End();
}

// Releases any used resources.
void ds::state::CreditsState::CleanUp() {
}

// Returns the state to change to.
ds::state::GameState ds::state::CreditsState::GetNextState() {
	return GameState::GS_MenuState;
}
