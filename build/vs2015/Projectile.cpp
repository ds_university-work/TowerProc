#include "Projectile.h"

// --CONSTRUCTORS--

ds::Projectile::Projectile() {
}

ds::Projectile::Projectile(gef::Mesh* mesh, gef::Matrix44* anchor, gef::Vector4 position, gef::Vector4* worldScale) : GameObject3D(mesh, anchor, position, worldScale, false) {
}

ds::Projectile::~Projectile() {
}

// -----PUBLIC-----

// Initialises the projectile.
void ds::Projectile::Initialise(ds::Enemy* enemyTarget, gef::Vector4 start, gef::Vector4 end, float speed, float damage_) {
	// Set attributes.
	target = enemyTarget;
	startPosition = start;
	endPosition = end;
	movementSpeed = speed;
	damage = damage_;

	// Initialise variables.
	lifetime = 0;
	destroy = false;
}

// Damages the enemy.
void ds::Projectile::DamageEnemy() {
	// Destroy projectile, damage enemy.
	target->health -= damage;
	destroy = true;
}

// -----PRIVATE----

// Updates the projectile.
void ds::Projectile::OverrideableUpdate(float deltaTime) {
	// Get distance between projectile and target.
	float distance = ds::utils::GetDistanceBetweenPoints(GetPosition(), endPosition);

	if (distance < 0.01f) {
		// Damage the enemy.
		DamageEnemy();
	} else if (lifetime > 2) {
		// Set the projectile to be destroyed.
		destroy = true;
	} else {
		// Update target position.
		endPosition = target->GetPosition(false) - AnchorTransform->GetTranslation();
		lifetime += deltaTime * 0.5f * movementSpeed;

		// Lerp towards target point.
		gef::Vector4 newPosition;
		newPosition.Lerp(startPosition, endPosition, lifetime);
		SetPosition(newPosition);
	}
}