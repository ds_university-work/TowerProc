#ifndef ARCAMERA_H
#define ARCAMERA_H

#include <vector>

#include <graphics/sprite.h>
#include <graphics/sprite_renderer.h>
#include <platform/vita/graphics/texture_vita.h>
#include <sony_sample_framework.h>
#include <system\platform.h>

#include "Marker.h"

// Marker macros
#define AR_MARKER_1	0
#define AR_MARKER_2	1
#define AR_MARKER_3	2
#define AR_MARKER_4	3
#define AR_MARKER_5	4
#define AR_MARKER_6	5

namespace ds {

// Class for storing data from the augmented reality camera.
// ================
class ARCamera {
public:
	ARCamera();
	~ARCamera();

	void SetUpARCamera(gef::Platform* platform);
	void UpdateMarkers();
	void ResetCameraWhiteBalance();
	void RenderARCameraFeed(AppData* dat, gef::SpriteRenderer* spriteRenderer);
	void CalibrateGridMarkers();

	// Getters
	ds::Marker* GetMarker(int index);
	gef::Vector2 GetGridMarkerDistance();

	gef::Matrix44 projectionMatrix3D;
	gef::Matrix44 viewMatrix3D;

	bool gridMarkerVisibilityChange;

private:
	gef::Sprite cameraFeed;
	gef::TextureVita cameraTexture;

	float cameraScalingFactor;
	gef::Matrix44 orthoCameraProjectionMatrix;

	std::vector<ds::Marker> markers;

	gef::Vector2 gridMarkerDistance;
};

}

#endif