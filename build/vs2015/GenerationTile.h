#ifndef GENERATIONTILE_H
#define GENERATIONTILE_H

#include "maths/vector2.h"

namespace ds {
namespace procgen {

// Enum for defining a tile's current state/type.
// ================
enum GenerationTileType {
	Invalid,
	PlayerHome,
	EnemySpawn,
	Visited,
	Unvisited,
	Path
};

// Tile class for use during level maze generation.
// ================
class GenerationTile {
public:
	GenerationTile(int x, int y);
	~GenerationTile();

	GenerationTile* parentTile;
	gef::Vector2 gridCoords;
	GenerationTileType type;
};

}
}

#endif