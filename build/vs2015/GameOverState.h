#ifndef GAMEOVERSTATE_H
#define GAMEOVERSTATE_H

#include "BaseState.h"

// ===============
//   DEPRECRATED
// ===============

namespace ds {
namespace state {

// Game over screen game state.
// ================
class GameOverState : public ds::state::BaseState {
public:
	GameOverState();
	~GameOverState();

	void Initialise();
	void Update(float deltaTime, AppData* dat);
	void Render(AppData* dat);
	void CleanUp();

	GameState GetNextState();

private:

};

}
}

#endif