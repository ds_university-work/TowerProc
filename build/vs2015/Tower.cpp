#include "Tower.h"

// --CONSTRUCTORS--

ds::Tower::Tower() {
}

ds::Tower::Tower(gef::Mesh* mesh, gef::Mesh* topMesh, gef::Matrix44* anchor, gef::Vector4 position, gef::Vector4* worldScale) : GameObject3D(mesh, anchor, position, worldScale, false) {
	// Initialise top of tower.
	top = GameObject3D(topMesh, GetWorldTransform(), gef::Vector4(0, 0, 1), worldScale, true);
	top.SetScale(gef::Vector4(60.0f, 60.0f, 10.0f));
}

ds::Tower::~Tower() {
}

// -----PUBLIC-----

// Initialises the tower.
void ds::Tower::Initialise(ds::TowerType towerType, std::vector<ds::Projectile>* projectiles, ModelHandler* modelHandler_, AudioHandler* audioHandler_) {
	type = towerType;
	projectileArray = projectiles;
	modelHandler = modelHandler_;
	audioHandler = audioHandler_;

	// Set attributes depending on type of tower.
	switch (type) {
		case TowerType::Gun:
			range = 0.075f;
			fireDelay = 0;
			damage = 0.8f;
			break;
		case TowerType::Bomb:
			range = 0.1f;
			fireDelay = 0;
			damage = 4;
			break;
		default:
			// Do nothing.
			break;
	}

	// Initialise target.
	target = NULL;

	// Anchor the tower top section.
	top.SetWorldAnchor(GetWorldTransform());

	// Initialise variables.
	shotsFired = 0;
}

// Sets the tower's target.
void ds::Tower::SetTarget(ds::Enemy* enemy) {
	target = enemy;
}

// Updates the tower's target.
void ds::Tower::UpdateTarget(std::vector<ds::Enemy>* enemies) {
	// Remove tower target.
	target = NULL;

	// Loop through all enemies.
	for (int i = 0; i < enemies->size(); i++) {
		// If enemy is within range, set enemy as target.
		if (ds::utils::GetDistanceBetweenPoints(GetPosition(false), enemies->at(i).GetPosition(false)) < range) {
			target = &enemies->at(i);
			break;
		}
	}
}

// -----PRIVATE----

// Updates the tower.
void ds::Tower::OverrideableUpdate(float deltaTime) {
	top.SetWorldAnchor(GetWorldTransform());

	// Update the top section.
	top.Update(deltaTime);

	// Fire a projectile if able to.
	if (fireDelay <= 0) {
		Fire();
	} else {
		fireDelay -= deltaTime;
	}
}

// Fires a projectile at the target.
void ds::Tower::Fire() {
	// If tower has a target.
	if (target != NULL) {
		// Create projectile and set it with instructions to reach target.
		projectileArray->push_back(Projectile(modelHandler->GetMesh("Cube"), GetWorldTransform(), gef::Vector4(0, 0, 0), worldScale));

		gef::Vector4 relativeTargetPosition = target->GetPosition(false) - GetPosition(false);
		projectileArray->back().Initialise(target, gef::Vector4(0, 0, 0), relativeTargetPosition, 20.0f, damage);
		projectileArray->back().SetScale(0.1f);

		// TODO: Rotate tower top towards enemy.

		// Reset the delay for firing.
		ResetFireDelay();
		shotsFired++;
		audioHandler->PlaySample("Gunfire");
	}
}

// Resets the fire delay.
void ds::Tower::ResetFireDelay() {
	switch (type) {
		case TowerType::Gun:
			fireDelay = 0.4f;
			break;
		case TowerType::Bomb:
			fireDelay = 2.0f;
			break;
		default:
			// Do nothing.
			break;
	}
}