#ifndef DEBUG_H
#define DEBUG_H

#include <string>
#include <vector>

#include <graphics/font.h>
#include <graphics/sprite_renderer.h>
#include <maths/vector2.h>
#include <maths/vector4.h>

namespace ds {
namespace utils {

// Text struct for storing debug text to be rendered.
// ================
struct Text {
public:
	std::string text;
	gef::Vector4 position;
	unsigned int colour;
	float scale;
};

// Debug class for rendering debug text and sprites to the screen.
// ================
class Debug {
public:
	Debug();
	~Debug();

	void Render(gef::SpriteRenderer* spriteRenderer, gef::Font* font);
	void AddText(std::string text, gef::Vector2 position, unsigned int colour = 0xffffff, float scale = 1.0f);

	bool enabled;

private:
	std::vector<Text> debugText;

};

}
}

#endif