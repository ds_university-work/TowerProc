#ifndef STATEHANDLER_H
#define STATEHANDLER_H

#include <graphics/font.h>
#include <graphics/renderer_3d.h>
#include <input/input_manager.h>
#include <input/keyboard.h>
#include <system/platform.h>

#include "AudioHandler.h""
#include "Debug.h"
#include "ModelHandler.h"

#include "BaseState.h"
#include "SplashState.h"
#include "MenuState.h"
#include "InGameState.h"
#include "GameOverState.h"
#include "CreditsState.h"

namespace ds {
namespace state {

// State handler class for managing the game states.
// ================
class StateHandler {
public:
	StateHandler();
	~StateHandler();

	void Initialise(gef::Platform* platform_);
	void Update(float deltaTime, AppData* dat);
	void Render(AppData* dat);
	void CleanUp();

private:
	void LoadGameAssets();

	// Game Handlers
	gef::Platform* platform;
	gef::SpriteRenderer* spriteRenderer;
	gef::Renderer3D* renderer3D;
	gef::InputManager* inputManager;
	ds::AudioHandler* audioHandler;
	ds::ModelHandler* modelHandler;
	ds::TextureHandler* textureHandler;
	ds::utils::Debug* debugOverlay;
	ds::ARCamera* arCamera;

	// Game Resources
	gef::Font* font;

	// Game States
	ds::state::BaseState* pCurrentState;
	ds::state::SplashState splashState;
	ds::state::MenuState menuState;
	ds::state::InGameState inGameState;
	ds::state::GameOverState gameOverState;
	ds::state::CreditsState creditsState;

	// Game Variables
	GameState currentStateEnum;
	float framerate;
	bool markersLost;

};

}
}

#endif