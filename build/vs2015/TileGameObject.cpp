#include "TileGameObject.h"

// --CONSTRUCTORS--
ds::TileGameObject::TileGameObject() {
}

ds::TileGameObject::TileGameObject(gef::Mesh* mesh, gef::Matrix44* anchor, gef::Vector4 position, gef::Vector4* worldScale) : GameObject3D(mesh, anchor, position, worldScale) {
}

ds::TileGameObject::~TileGameObject() {
}

// -----PUBLIC-----

// Initialises the tile settings.
void ds::TileGameObject::Initialise(gef::Vector2 gridCoordinates, TileType tileType) {
	gridCoords = gridCoordinates;
	type = tileType;

	installedTower = NULL;
}

// Places a tower on the tile if possible.
bool ds::TileGameObject::PlaceTower(ds::ModelHandler* modelHandler, ds::AudioHandler* audioHandler, std::vector<ds::Tower>* towers, ds::TowerType towerType, std::vector<Projectile>* projectiles) {
	if (CanPlaceTower()) {
		// Get the name of the model.
		std::string modelName;
		switch (towerType) {
			case TowerType::Gun:
				modelName = "GunTowerFull";
				break;
			case TowerType::Bomb:
				modelName = "BombTower";
				break;
		}

		// Create the tower.
		towers->push_back(ds::Tower(modelHandler->GetMesh("GunTowerBase"), modelHandler->GetMesh("GunTowerTop"), GetWorldTransform(), gef::Vector4(0, 0, 0.0f), worldScale));
		towers->back().SetScale(gef::Vector4(1.0f, 1.0f, 2.0f));
		towers->back().Initialise(TowerType::Gun, projectiles, modelHandler, audioHandler);

		// Link newly created tower to the game object pointer.
		installedTower = &towers->back();

		return true;
	} else {
		return false;
	}
}

// Sets the type of tile.
void ds::TileGameObject::SetTileType(ds::TileType t) {
	type = t;
}

// Returns the grid coordinates of the tile.
gef::Vector2 ds::TileGameObject::GetGridCoords() {
	return gridCoords;
}

// Returns the type of tile.
ds::TileType ds::TileGameObject::GetTileType() {
	return type;
}

// Returns the installed tower, if it exists.
ds::GameObject3D* ds::TileGameObject::GetTower() {
	return installedTower;
}

// Updates the tile game object.
void ds::TileGameObject::OverrideableUpdate(float deltaTime) {
}

// Checks whether a tower can be placed on the tile.
bool ds::TileGameObject::CanPlaceTower() {
	// Make sure the tower is being built on a valid tile.
	if (type != TileType::Ground)
		return false;

	// Make sure there isn't already an installed tower.
	if (installedTower != NULL)
		return false;

	return true;
}