#ifndef COLLISION_H
#define COLLISION_H

#include <graphics/mesh.h>
#include <graphics/mesh_instance.h>

#include "GameObject3D.h"

namespace ds {
namespace utils {

bool CheckCollisionAABB(gef::MeshInstance* meshA, gef::MeshInstance* meshB);
bool CheckCollisionSphere(GameObject3D goA, GameObject3D goB, bool innerBounds = true);
float GetDistanceBetweenPoints(gef::Vector4 a, gef::Vector4 b);

}
}

#endif