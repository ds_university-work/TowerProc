#include "GameObject3D.h"

// --CONSTRUCTORS--

ds::GameObject3D::GameObject3D() {
}

ds::GameObject3D::GameObject3D(gef::Mesh* mesh, gef::Matrix44* anchor, gef::Vector4 position, gef::Vector4* worldScale_, bool anchorScale) {
	// Set world scale.
	worldScale = worldScale_;

	// Initalise gameobject transform.
	set_mesh(mesh);
	LocalTransform.SetIdentity();
	SetWorldAnchor(anchor);
	SetPosition(position);
	SetScale(1.0f);
	SetRotation(gef::Vector4(0.0f, 0.0f, 0.0f));
	useAnchorScale = anchorScale;
}

ds::GameObject3D::~GameObject3D() {

}

// -----PUBLIC-----

// Updates the base game object functionality and runs the child class update.
void ds::GameObject3D::Update(float deltaTime) {
	// Run the child class functionality.
	OverrideableUpdate(deltaTime);

	// Run the base class functionality.
	UpdateWorldTransform();
}

// Sets the game object's absolute position.
void ds::GameObject3D::SetPosition(gef::Vector4 position) {
	LocalPosition = position;

	UpdateWorldTransform();
}

// Adjusts the position of the game object by given vector.
void ds::GameObject3D::AdjustPosition(gef::Vector4 positionDelta) {
	LocalPosition += positionDelta;

	UpdateWorldTransform();
}

// Sets the game object's absolute x rotation.
void ds::GameObject3D::SetRotationX(float degrees) {
	// Convert to radians
	float rad = degrees * 3.1415f / 180.f;

	gef::Matrix44 xRotation;
	xRotation.RotationX(rad);

	LocalRotation = LocalRotation * xRotation;

	UpdateWorldTransform();
}

// Sets the game object's absolute y rotation.
void ds::GameObject3D::SetRotationY(float degrees) {
	// Convert to radians
	float rad = degrees * 3.1415f / 180.f;

	gef::Matrix44 yRotation;
	yRotation.RotationY(rad);

	LocalRotation = LocalRotation * yRotation;

	UpdateWorldTransform();
}

// Sets the game object's absolute z rotation.
void ds::GameObject3D::SetRotationZ(float degrees) {
	// Convert to radians
	float rad = degrees * 3.1415f / 180.f;

	gef::Matrix44 zRotation;
	zRotation.RotationZ(rad);

	LocalRotation = LocalRotation * zRotation;

	UpdateWorldTransform();
}

// Sets the game object's absolute rotation.
void ds::GameObject3D::SetRotation(gef::Vector4 rotation) {
	gef::Matrix44 x, y, z;
	x.RotationX(rotation.x());
	y.RotationY(rotation.y());
	z.RotationZ(rotation.z());

	LocalRotation = x * y * z;
}

// Sets the game object's rotation matrix.
void ds::GameObject3D::SetRotationMatrix(gef::Matrix44 rotation) {
	LocalRotation = rotation;
}

// Sets the game objects absolute scaling; returns true if successful.
bool ds::GameObject3D::SetScale(gef::Vector4 scale) {
	if (scale.x() != 0 && scale.y() != 0 && scale.z() != 0) {
		LocalScale.Scale(scale);
	} else {
		return false;
	}

	UpdateWorldTransform();

	return true;
}

// Sets the game objects absolute scaling; returns true if successful.
bool ds::GameObject3D::SetScale(float scale) {
	return SetScale(gef::Vector4(scale, scale, scale));
}

// Sets the world anchor transform.
void ds::GameObject3D::SetWorldAnchor(gef::Matrix44* anchor, bool anchorScale) {
	AnchorTransform = anchor;
	useAnchorScale = anchorScale;

	UpdateWorldTransform();
}

// Returns the game objects mesh.
gef::Mesh ds::GameObject3D::GetMesh() {
	return *mesh();
}

// Returns the game objects position.
gef::Vector4 ds::GameObject3D::GetPosition(bool local) {
	if (local)
		return LocalTransform.GetRow(3);
	else
		return transform().GetRow(3);
}

// Returns the game objects world transform.
gef::Matrix44* ds::GameObject3D::GetWorldTransform() {
	return &transform_;
}

// Returns the game objects local scale.
gef::Vector4 ds::GameObject3D::GetLocalScale() {
	return LocalScale.GetScale();
}

// -----PRIVATE----

// Updates the world transform of the game object based on the local and anchor transforms.
void ds::GameObject3D::UpdateWorldTransform() {
	UpdateLocalTransform();

	// Check if there is an anchor transform
	if (AnchorTransform != NULL) {
		gef::Matrix44 anchTransform = *AnchorTransform;

		if (!useAnchorScale) {
			anchTransform.Scale(gef::Vector4(1.0f / AnchorTransform->GetScale().x(),
				1.0f / AnchorTransform->GetScale().y(),
				1.0f / AnchorTransform->GetScale().z(),
				0));

			anchTransform = anchTransform * *AnchorTransform;
		}

		//set_transform(LocalTransform * anchTransform * worldScaleTransform);
		set_transform(LocalTransform * anchTransform);
	} else {
		//set_transform(LocalTransform * worldScaleTransform);
		set_transform(LocalTransform);
	}
}

// Updates the local transform of the game object based upon the position, rotation and scaling.
void ds::GameObject3D::UpdateLocalTransform() {
	// Set the world scale matrix.
	gef::Matrix44 worldScaleTransform;
	worldScaleTransform.Scale(*worldScale);

	// Calculate the local transform.
	LocalTransform = LocalRotation * LocalScale * worldScaleTransform;
	LocalTransform.SetTranslation(LocalPosition);
}

// Overrideable function for child classes.
void ds::GameObject3D::OverrideableUpdate(float deltaTime) {

}
