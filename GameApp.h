#ifndef GAMEAPP_H
#define GAMEAPP_H

#include <vector>

#include <system/application.h>

// Vita AR includes
#include <camera.h>
#include <gxm.h>
#include <motion.h>
#include <libdbg.h>
#include <libsmart.h>

#include "build/vs2015/StateHandler.h"


// FRAMEWORK FORWARD DECLARATIONS
namespace gef
{
	class Platform;
	class SpriteRenderer;
	class Font;
	class Renderer3D;
	class Mesh;
	class RenderTarget;
	class TextureVita;
	class InputManager;
	class Keyboard;
}

// Game application class.
// ================
class GameApp : public gef::Application {
public:
	GameApp(gef::Platform& platform);

	void Init();
	void CleanUp();
	bool Update(float frame_time);
	void Render();

private:
	ds::state::StateHandler stateHandler;
};

#endif