#include "GameApp.h"
#include <system/platform.h>
#include <graphics/sprite_renderer.h>
#include <graphics/texture.h>
#include <graphics/mesh.h>
#include <graphics/primitive.h>
#include <assets/png_loader.h>
#include <graphics/image_data.h>
#include <graphics/font.h>
#include <input/touch_input_manager.h>
#include <maths/vector2.h>
#include <input/input_manager.h>
#include <maths/math_utils.h>
#include <graphics/renderer_3d.h>
#include <graphics/render_target.h>
#include <system/debug_log.h>

#include <sony_sample_framework.h>
#include <sony_tracking.h>
#include <string>

// --CONSTRUCTORS--

GameApp::GameApp(gef::Platform& platform) : Application(platform) {
}

// -----PUBLIC-----

// Initialises the game.
void GameApp::Init() {
	// Initialise sony framework.
	sampleInitialize();
	smartInitialize();

	// Reset marker tracking.
	AppData* dat = sampleUpdateBegin();
	smartTrackingReset();
	sampleUpdateEnd(dat);

	// Initialise the state handler.
	stateHandler.Initialise(&platform_);
}

// Cleans up any used resources.
void GameApp::CleanUp() {
	// Clean up sony resources.
	smartRelease();
	sampleRelease();

	// Clean up resources.
	stateHandler.CleanUp();
}

// Updates the game.
bool GameApp::Update(float frame_time) {
	AppData* dat = sampleUpdateBegin();

	// Use the tracking library to try and find markers.
	smartUpdate(dat->currentImage);

	// Update the game.
	stateHandler.Update(frame_time, dat);

	sampleUpdateEnd(dat);

	return true;
}

// Renders the game to the screen.
void GameApp::Render() {
	AppData* dat = sampleRenderBegin();

	// Render the game.
	stateHandler.Render(dat);

	sampleRenderEnd();
}
